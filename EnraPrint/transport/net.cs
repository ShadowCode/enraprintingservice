﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace EnraPrint.transport
{
    public class Net : ITransport
    {
        private bool state;
        private Socket sock;

        public void CloseDevice()
        {
            state = false;
            sock.Shutdown(SocketShutdown.Both);
            sock.Dispose();
            sock = null;
        }

        public async Task<bool> OpenDevice(string id)
        {
            // Parse the address
            var address = id.Split(':');
            // Create a socket
            var _sock = new Socket(SocketType.Stream, ProtocolType.Tcp);
            var sock_args = new SocketAsyncEventArgs();
            sock_args.RemoteEndPoint = new IPEndPoint(
                IPAddress.Parse(address[0]), 
                int.Parse(address[1])
                );
            sock_args.Completed += ConnectionCallback;
            // Connect the socket
            state = sock.ConnectAsync(sock_args);
            if (state)
                sock = sock_args.ConnectSocket;

            return state;
        }

        private void ConnectionCallback(object sender, SocketAsyncEventArgs e)
        {
            if (e.ConnectSocket != null)
            {
                sock = e.ConnectSocket;
                state = true;
            }
            else
                state = false;
        }

        public async Task<bool> SendData(ByteBuffer data)
        {
            var args = new SocketAsyncEventArgs();
            args.SetBuffer(data.contents, 0, data.size);
            return sock.SendAsync(args);
        }
    }
}
