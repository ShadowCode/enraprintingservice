﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnraPrint
{
    public class ByteBuffer
    {
        const int KDefaultSize = 0x1000;
        const int KAllocStepSize = 0x200;

        private byte[] buffer;
        public int alloc_size { get; set; }
        private int write_pos;
        public int WritePos {
            get { return write_pos; }
            set { System.Diagnostics.Debug.Assert(value <= this.alloc_size); this.write_pos = value; }
        }
        private int read_pos;
        public int ReadPos {
            get { return read_pos; }
            set { System.Diagnostics.Debug.Assert(value <= this.write_pos); this.read_pos = value; }
        }
        private bool _owns_buffer;

        public static ByteBuffer Create()
        {
            return ByteBuffer.Create(KDefaultSize);
        }

        public static ByteBuffer Create(int reserve)
        {
            ByteBuffer self = new ByteBuffer();
            self._owns_buffer = true;
            self.Reserve(reserve);
            return self;
        }
        public static ByteBuffer Create(ByteBuffer copy)
        {
            return ByteBuffer.Create(copy.buffer, copy.write_pos);
        }
        public static ByteBuffer Create(string str)
        {
            ByteBuffer self = new ByteBuffer();

            self._owns_buffer = true;
            var _tmp = Encoding.UTF8.GetBytes(str);
            self.Write(_tmp, 0, _tmp.Length);
            return self;
        }
        public static ByteBuffer Create(byte[] data, int len)
        {
            ByteBuffer self = new ByteBuffer();

            self._owns_buffer = true;
            self.Write(data, 0, len);
            return self;
        }
        public static ByteBuffer CreateAbsorbe(byte[] data, int len)
        {
            ByteBuffer self = new ByteBuffer();

            self._owns_buffer = false;
            self.Absorbe(data, len);
            return self;
        }

        public static void Destroy(ByteBuffer self)
        {
            if (self._owns_buffer)
            {
                if (self.buffer != null)
                    self.buffer = null;
            }
        }

        public void Clear() {
            this.write_pos = 0;
            this.read_pos = 0;
        }
        public void Reset() {
            this.read_pos = 0;
        }

        public void Reserve(int reserve)
        {
            System.Diagnostics.Debug.Assert(reserve < 10000000);// Note: this is flud protection, if the bufers are not read only writen this will fire
            System.Diagnostics.Debug.Assert(this._owns_buffer);

            if (reserve > this.alloc_size)
            {
                if (this.buffer != null)
                {
                    var _buff = new byte[reserve];
                    this.buffer.CopyTo(_buff, 0);
                    this.buffer = _buff;
                }
                else
                    this.buffer = new byte[reserve];

                this.alloc_size = reserve;
            }
        }

        public void Reserve(int reserve, byte val)
        {
            this.Reserve(reserve);
            this.write_pos = reserve;
            for (int i = 0; i < this.buffer.Length; ++i)
            {
                this.buffer[i] = val;
            }
        }
        public int Crunch()
        {
            int crunch_amount = this.read_pos;
            if (crunch_amount > 0)
            {
                int len = this.bytes_left;
                Buffer.BlockCopy(this.buffer, this.read_pos, buffer, 0, len);
                this.read_pos = 0;
                this.write_pos -= crunch_amount;
            }
            return crunch_amount;
        }
        public void CrunchReserve(int needed)
        {
            if (this._owns_buffer)
            {
                int size_needed = this.write_pos + needed;
                if (size_needed > this.alloc_size)
                {
                    size_needed -= this.Crunch();
                    this.Reserve(size_needed + KAllocStepSize);
                }
            }
        }

        public void Absorbe(ByteBuffer src)
        {
            if (this._owns_buffer)
            {
                if (this.buffer != null)
                    this.buffer = null;
            }
            this.buffer = src.buffer;
            this.write_pos = src.write_pos;
            this.read_pos = src.read_pos;
            this.alloc_size = src.alloc_size;
            src._owns_buffer = false;
            this._owns_buffer = true;
        }
        public void Absorbe(byte[] data, int len)
        {
            if (this._owns_buffer)
            {
                if (this.buffer != null)
                    this.buffer = null;
            }
            this.buffer = data;
            this.alloc_size = len;
            this.write_pos = len;
            this.read_pos = 0;
            this._owns_buffer = false;
        }

        public static void Copy(ByteBuffer des, ByteBuffer src)
        {
            des.CrunchReserve(src.write_pos);
            des.Write(src.buffer, src.read_pos, src.bytes_left);
        }

        public void Write(byte[] data, int index, int len)
        {
            this.CrunchReserve(len);
            Buffer.BlockCopy(data, index, this.buffer, this.write_pos, len);
            this.write_pos += len;
        }
        public bool Read(out byte[] data, int index, int len)
        {
            int _bytes_left = this.bytes_left;
            if (_bytes_left < len)
            {
                // Note: if we dont have the right size to read just wait, dont partialy read
                /*memcpy( data, (uint8*)self->buffer + self->read_pos, bytes_left );
                self->read_pos += bytes_left;*/
                data = null;
                return false;
            }
            else
            {
                data = new byte[len];
                Array.Copy(this.buffer, this.read_pos, data, index, len);
                this.read_pos += len;
                return true;
            }
        }

        public void WriteStr(string str)
        {
            this.Write(Encoding.ASCII.GetBytes(str), 0, str.Length);
        }

        public int size { get { return (this.write_pos); } }
        public int bytes_left { get { return (this.write_pos - this.read_pos); } }

        public byte[] contents { get { return this.buffer; } }
        public char[] chars { get {
                char[] chars = new char[this.buffer.Length / sizeof(char)];
                System.Buffer.BlockCopy(this.buffer, 0, chars, 0, this.buffer.Length);
                return chars; }
        }
    }
}
