﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnraPrint
{
    public class PrinterProtocol
    {
        protected volatile byte printing_mode;
        public volatile bool InlineMode = true;
        public volatile bool TranscodeCirilic = false;

        public virtual ByteBuffer NewLine(ByteBuffer _out)
        {
            return _out;
        }

        // Moves the print position to the next horizontal tab position.
        public virtual ByteBuffer HorizontalTab(ByteBuffer _out)
        {
            return _out;
        }

        // Find next black mark
        public virtual ByteBuffer NextMark(ByteBuffer _out)
        {
            return _out;
        }

        // Calibrate black mark
        public virtual ByteBuffer CalibrateBlackMark(ByteBuffer _out)
        {
            return _out;
        }

        //Prints the data in the print buffer and feeds one line based on the current line
        //spacing.
        public virtual ByteBuffer PrintAndLineFeed(ByteBuffer _out)
        {
            return _out;
        }

        // Prints the data in the print buffer collectively and returns to standard mode.
        public virtual ByteBuffer PrintAndReturn(ByteBuffer _out)
        {
            return _out;
        }

        // When automatic line feed is enabled, this command functions the same as LF;
        public virtual ByteBuffer PrintAndCarriageReturn(ByteBuffer _out)
        {
            return _out;
        }

        // Transmits the selected printer status specified by n in real-time, according to the
        // following parameters:
        //	n = 1: Transmit printer status
        //	n = 2: Transmit off-line status
        //	n = 3: Transmit error status
        //	n = 4: Transmit paper roll sensor status
        public virtual ByteBuffer RealTimeStatusTransmission(byte n, ByteBuffer _out)
        {
            return _out;
        }

        // In page mode, prints all buffered data in the printing area collectively.
        public virtual ByteBuffer PrintDataInPageMode(ByteBuffer _out)
        {
            return _out;
        }

        // Sets the character spacing for the right side of the character to [ n ´ horizontal or
        // vertical motion units].
        public virtual ByteBuffer SetRightSideChSpacing(byte n, ByteBuffer _out)
        {
            return _out;
        }

        //	Selects print mode(s) using n as follows:
        //	Bit Off/On 	Hex Decimal Function
        //	0 	Off 	00 	0 		Character font A (12 ╳ 24).
        //		On 		01 	1 		Character font B (9 ╳ 17).
        //	1 	- 		- 	- 		Undefined.
        //	2 	- 		- 	- 		Undefined.
        //	3 	Off 	00 	0 		Emphasized mode not selected.
        //		On 		08 	8 		Emphasized mode selected.
        //	4 	Off 	00 	0 		Double-height mode not selected.
        //		On 		10 	16 		Double-height mode selected.
        //	5 	Off 	00 	0 		Double-width mode not selected.
        //		On 		20 	32 		Double-width mode selected.
        //	6 	- 		- 	- 		Undefined.
        //	7 	Off 	00 	0 		Underline mode not selected.
        //		On 		80 	128 	Underline mode selected.
        public virtual ByteBuffer SelectPrintMode(byte n, ByteBuffer _out)
        {
            return _out;
        }

        // Sets the distance from the beginning of the line to the position at which
        // subsequent characters are to be printed.
        // The distance from the beginning of the line to the print position is
        // [( length + height ╳ 256) ╳(vertical or horizontal motion unit)] inches.
        public virtual ByteBuffer SetAbsolutePosition(byte length, byte height, ByteBuffer _out)
        {
            return _out;
        }

        // Selects a bit-image mode using m for the number of dots specified by length and height.
        // Note: To create the data for this command make an extension method handling the dot encoding.
        public virtual ByteBuffer SelectBitImageMode(byte m, byte length, byte height, ByteBuffer data, ByteBuffer _out) {
		    return _out;
        }

        // Selects 1/ 6-inch line (approximately 4.23mm) spacing.
        public virtual ByteBuffer SetDefaultLineSpacing(ByteBuffer _out)
        {
            return _out;
        }

        // Sets the line spacing to [ n ╳ vertical or horizontal motion unit] inches.
        public virtual ByteBuffer SetLineSpacing(byte n, ByteBuffer _out)
        {
            return _out;
        }

        // Clears the data in the print buffer and resets the printer mode to the mode that
        // was in effect when the power was turned on.
        public virtual ByteBuffer InitPrinter(ByteBuffer _out)
        {
            return _out;
        }

        // Sets horizontal tab positions.
        // • n specifies the column number for setting a horizontal tab position from the
        // beginning of the line.
        // • k indicates the total number of horizontal tab positions to be set.
        public virtual ByteBuffer SetHorizontalTabPositions(ByteBuffer tab_positions, ByteBuffer _out)
        {
            return _out;
        }

        // Turns emphasized mode on or off
        public virtual ByteBuffer EmphasizedMode(bool mode, ByteBuffer _out)
        {
            return _out;
        }

        // Turns double-strike mode on or off.
        public virtual ByteBuffer DoubleStrikeMode(bool mode, ByteBuffer _out)
        {
            return _out;
        }

        // Prints the data in the print buffer and feeds the paper [ n ╳ vertical or horizontal
        // motion unit] inches.
        public virtual ByteBuffer PrintAndFeedPaperByInches(byte inches, ByteBuffer _out)
        {
            return _out;
        }

        // Prints the data in the print buffer and feeds n lines.
        public virtual ByteBuffer PrintAndFeedPaper(byte lines, ByteBuffer _out)
        {
            return _out;
        }

        // Selects character fonts.
        // font = 0, 1, 48, 49
        public virtual ByteBuffer SelectChFont(byte font, ByteBuffer _out)
        {
            return _out;
        }

        // Selects an international character set n from the following table:
        // n Character
        // 0 U.S.A.
        // 1 France
        // 2 Germany
        // 3 U.K.
        // 4 Denmark
        // 5 Sweden
        // 6 Italy
        // 7 Spain
        // 8 Japan
        // 9 Norway
        // 10 Denmark
        // 11 Spain
        // 12 Latin
        // 13 Korea
        // 14 Slovenia/Croatia
        // 15 Chinese
        // The character sets for Slovenia/Croatia and China are ZJported only in the
        // Simplified Chinese model.
        public virtual ByteBuffer SelectInternationalLanguage(byte n, ByteBuffer _out)
        {
            return _out;
        }

        // Sets the print starting position based on the current position by using the
        // horizontal or vertical motion unit.
        // • This command sets the distance from the current position to [( nL + nH ╳ 256) ╳
        // horizontal or vertical motion unit]
        public virtual ByteBuffer SetRelativePosition(byte lenght, byte height, ByteBuffer _out)
        {
            return _out;
        }

        // Aligns all the data in one line to the specified position
        // Note: this commands are valid only after new line
        public virtual ByteBuffer AlignContentLeft(ByteBuffer _out)
        {
            return _out;
        }
        public virtual ByteBuffer AlignContentCenter(ByteBuffer _out)
        {
            return _out;
        }
        public virtual ByteBuffer AlignContentRight(ByteBuffer _out)
        {
            return _out;
        }

        // Selects a page n from the character code table.
        // n Page
        // 0 PC437 [U.S.A.Standard Europe]
        // 1 Katakana
        // 2 PC850:Multilingual
        // 3 PC860:Portuguese
        // 4 PC863 [Canadian French]
        // 5 PC865:Nodic
        // 6 West Europe
        // 7 Greek
        // 8 Hebrew
        // 9 PC755:East Europe
        // 10 Iran
        // 16 WPC1252
        // 17 PC866:Cyrillic#2
        // 18 PC852:Latin2
        // 19 PC858
        // 20 IranII
        // 21 Latvian
        public virtual ByteBuffer SelectCharecterTable(byte n, ByteBuffer _out)
        {
            return _out;
        }

        // Prints a NV bit image n using the mode specified by size.
        // 0 Normal
        // 1 Double-width
        // 2 Double-height
        // 3 Quadruple
        public virtual ByteBuffer PrintNVBitImage(byte n, byte size, ByteBuffer _out)
        {
            return _out;
        }

        // Select character size
        public virtual ByteBuffer SelectChSize(byte size, ByteBuffer _out)
        {
            return _out;
        }

        // Turns on or off white/black reverse printing mode.
        public virtual ByteBuffer InverseColors(bool flag, ByteBuffer _out)
        {
            return _out;
        }

        // Sets the left margin using n.
        public virtual ByteBuffer PrintableAreaLeftMargin(byte n, ByteBuffer _out)
        {
            return _out;
        }

        public virtual ByteBuffer SetPrinterWidth(byte n, ByteBuffer _out)
        {
            return _out;
        }

        // Executes paper cutting.
        public virtual ByteBuffer CutPaper(ByteBuffer _out)
        {
            return _out;
        }

        // Selects the height of the bar code.
        public virtual ByteBuffer SelectBarCodeHight(byte n, ByteBuffer _out)
        {
            return _out;
        }

        // Selects a bar code system and prints the bar code.
        // 65 UPC-A 11 ≤ n ≤ 12 48 ≤ d ≤ 57
        // 66 UPC-E 11 ≤ n ≤ 12 48 ≤ d ≤ 57
        // 67 JAN13 (EAN13) 12 ≤ n ≤ 13 48 ≤ d ≤ 57
        // 68 JAN8 (EAN8) 7 ≤ n ≤ 8 48 ≤ d ≤ 57
        // 69 CODE39 1 ≤ n ≤ 255
        // 45 ≤ d ≤ 57，
        // 65 ≤ d ≤ 90，
        // d = 32, 36, 37, 43, 45,
        // 46, 47
        // d = 42
        // 70 ITF 1 ≤ n ≤ 255（even number） 48 ≤ d ≤ 57
        // 71 CODABAR 1 ≤ n ≤ 255 48 ≤ d ≤ 57,
        // 65 ≤ d ≤ 68,
        // d = 36, 43, 45, 46,
        // 47, 58
        // 72 CODE93 1 ≤ n ≤ 255 0 ≤ d ≤ 127
        // 73 CODE128 2 ≤ n ≤ 255 0 ≤ d ≤ 127
        public virtual ByteBuffer PrintBarCode(byte system, ByteBuffer data, ByteBuffer _out)
        {
            return _out;
        }

        // Set the horizontal size of the bar code.
        // Note: 2 <= n <= 6 , Look at the specification
        public virtual ByteBuffer SetBarCodeWidth(byte n, ByteBuffer _out)
        {
            return _out;
        }

        //_outputs the pulse specified by t1 and t2 to connector pin m to open the chash drawer,
        //	as follows :
        //  m - 0, 48 pin2; 1, 49 pin5
        //  t1 - times x 2ms ON
        //  t2 - times x 2ms OFF
        public virtual ByteBuffer PulseToCD(byte t1, byte t2, byte m, ByteBuffer _out)
        {
            return _out;
        }
    }
}
