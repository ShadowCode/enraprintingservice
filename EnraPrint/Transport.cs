﻿
using System.Threading.Tasks;

namespace EnraPrint
{
    public interface ITransport
    {
        Task<bool> OpenDevice(string id);
        void CloseDevice();

        Task<bool> SendData(ByteBuffer data);
    }
}
