﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;

namespace EnraPrint
{
    namespace ViewModels
    {
        public class PairResponse
        {
            public string device_id { get; set; }
            public string device_ip { get; set; }
            public string order_printer { get; set; }
            public string rest_id { get; set; }
            public string rest_front_url { get; set; }
            public string rest_back_url { get; set; }
        }

        public class OrderResponse
        {

        }
    }

    public class PrintEngine
    {
        private ITransport transport_layer { get; set; }
        private PrinterProtocol protocol { get; set; }

        private ViewModels.PairResponse pair_data { get; set; }

        private string rest_url { get; set; }
        private string conf_rest_url { get; set; }
        public PrintEngine(string conf_rest_url)
        {
            this.conf_rest_url = conf_rest_url; // config.fast-menu.com
        }

        public async Task<bool> InitializeAsync()
        {
            // Pair the device
            var pair_request = WebRequest.CreateHttp(conf_rest_url + "/pair/device/" + await getGUID());
            using (var pair_response = await pair_request.GetResponseAsync())
            {
                var response = new StreamReader(pair_response.GetResponseStream()).ReadToEnd();
                pair_data = Newtonsoft.Json.JsonConvert.DeserializeObject<ViewModels.PairResponse>(response);
            }

            this.rest_url = pair_data.rest_back_url; // fmdev.fast-menu.com

            // Create transport and translation layers
            transport_layer = new transport.Usb();
            if (!await transport_layer.OpenDevice(pair_data.order_printer))
            {
                transport_layer = null;
                transport_layer = new transport.Net();
                if (!await transport_layer.OpenDevice(pair_data.order_printer))
                {
                    transport_layer = null;
                    System.Diagnostics.Debug.WriteLine("Unable to connect to any printer! \n");
                    return false;
                }
            }
            protocol = new Protocols.pp6900();

            return true;
        }

        private async Task<string> getGUID()
        {
            return await GetMAC();
        }

        public async Task<bool> UpdateAsync()
        {
            string last_order;
            // Get pending orders
            var order_request = WebRequest.CreateHttp(rest_url + "/orderService/oService.svc/CheckOrderForPrinting/" + this.pair_data.rest_id);
            order_request.ContentType = "application/json";
            order_request.Method = "POST";
            using (var order_response = await order_request.GetResponseAsync())
            {
                last_order = new StreamReader(order_response.GetResponseStream()).ReadToEnd();
                //last_order = "TEST   гр. София, ул. Д-р Атанас Москов 15   ";
            }
            if (string.IsNullOrWhiteSpace(last_order))
                return false;
            
            ByteBuffer response_data = ByteBuffer.Create(last_order);
            ByteBuffer print_data = ByteBuffer.Create();
            MarkupTranslator.Translate(protocol, response_data, print_data);
            
            return await this.transport_layer.SendData(print_data);
        }

        private async Task<String> GetMAC()
        {
            String MAC = null;
            StreamReader SR = await GetJsonStreamData("http://localhost:8080/api/networking/ipconfig");
            Windows.Data.Json.JsonObject ResultData = null;
            try
            {
                String JSONData;

                JSONData = SR.ReadToEnd();

                ResultData = (Windows.Data.Json.JsonObject)Windows.Data.Json.JsonObject.Parse(JSONData);
                Windows.Data.Json.JsonArray Adapters = ResultData.GetNamedArray("Adapters");

                //foreach (JsonObject Adapter in Adapters)   
                for (uint index = 0; index < Adapters.Count; index++)
                {
                    Windows.Data.Json.JsonObject Adapter = Adapters.GetObjectAt(index).GetObject();
                    String Type = Adapter.GetNamedString("Type");
                    if (Type.ToLower().CompareTo("ethernet") == 0)
                    {
                        MAC = ((Windows.Data.Json.JsonObject)Adapter).GetNamedString("HardwareAddress");
                        break;
                    }
                }
            }
            catch (Exception E)
            {
                System.Diagnostics.Debug.WriteLine(E.Message);
            }

            return MAC;
        }

        private async Task<StreamReader> GetJsonStreamData(String URL)
        {
            HttpWebRequest wrGETURL = null;
            Stream objStream = null;
            StreamReader objReader = null;

            try
            {
                wrGETURL = (HttpWebRequest)WebRequest.Create(URL);
                wrGETURL.Credentials = new NetworkCredential("Administrator", "p@ssw0rd");
                HttpWebResponse Response = (HttpWebResponse)(await wrGETURL.GetResponseAsync());
                if (Response.StatusCode == HttpStatusCode.OK)
                {
                    objStream = Response.GetResponseStream();
                    objReader = new StreamReader(objStream);
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("GetData " + e.Message);
            }
            return objReader;
        }
    }
}
