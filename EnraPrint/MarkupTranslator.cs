﻿using System;

namespace EnraPrint
{
    public static class MarkupTranslator
    {
        public static void Translate(PrinterProtocol protocol, ByteBuffer input, ByteBuffer output)
        {
            protocol.InitPrinter(output);
            protocol.SelectCharecterTable(28, output);
            for (; input.bytes_left > 0;)
            {
                byte[] ch;
                if (!input.Read(out ch, 0, 1))
                    break;
                if (ch[0] == ';')
                {
                    ch[0] = 0;
                    for (; ch[0] != ';' && input.Read(out ch, 0, 1);)
                    {
                        switch ((char)ch[0])
                        {
                            case 'b': // Bold
                                protocol.SelectPrintMode(32, output);
                                break;
                            case 'e': // Emphasizes
                                protocol.SelectPrintMode(5, output);
                                protocol.SetLineSpacing(10, output);
                                break;
                            case 'd': // Double height
                                protocol.SelectPrintMode(16, output);
                                break;
                            case 'u': // Underline
                                protocol.SelectPrintMode(128, output);
                                break;
                            case 'i': // Invert colors
                                protocol.InverseColors(true, output);
                                break;
                            case '1': // Size 1
                                protocol.SelectChSize(80, output);
                                break;
                            case '2': // Size 2
                                protocol.SelectChSize(64, output);
                                break;
                            case '3': // Size 3
                                protocol.SelectChSize(48, output);
                                break;
                            case '4': // Size 4
                                protocol.SelectChSize(32, output);
                                break;
                            case '5': // Size 5
                                protocol.SelectChSize(16, output);
                                break;
                            case '6': // Size 6
                                protocol.SelectChSize(0, output);
                                break;
                            case 'c': // Center
                                      //protocol->SelectPrintMode(0, out);
                                protocol.AlignContentCenter(output);
                                break;
                            case 'l': // Left
                                      //protocol->SelectPrintMode(0, out);
                                protocol.AlignContentLeft(output);
                                break;
                            case 'r': // Right
                                      //protocol->SelectPrintMode(0, out);
                                protocol.AlignContentRight(output);
                                break;
                            case 'n': // Normal
                                protocol.SelectPrintMode(0, output);
                                protocol.SelectChSize(0, output);
                                protocol.SetLineSpacing(0, output);
                                protocol.InverseColors(false, output);
                                //protocol->PrintAndReturn(out);
                                break;
                            case '_': // Bell
                                protocol.PulseToCD(200, 50, 1, output);
                                protocol.PulseToCD(200, 50, 0, output);
                                break;
                            case 'z':
                                protocol.SelectCharecterTable(28, output);
                                break;
                            case 'x': // Cut
                                protocol.NewLine(output);
                                protocol.CutPaper(output);
                                break;
                        }
                    }
                }
                else if ((char)ch[0] == '\n')
                {
                    protocol.NewLine(output);
                    protocol.InitPrinter(output);
                    protocol.SelectCharecterTable(28, output);
                }
                else
                {
                    if (((sbyte)ch[0]) < 0)
                    {
                        // Transcode
                        byte[] _utf_2;
                        input.Read(out _utf_2, 0, 1);
                        byte _transcoded = (byte)(UInt16)((((byte)ch[0] << 8) + (_utf_2[0])) - 53392/* + 48*/); // -53392
                        if (_transcoded < 164) _transcoded += 192;
                        output.Write(new byte[] { _transcoded }, 0, 1);
                    }
                    else
                    {
                        output.Write(ch, 0, 1);
                    }
                }
            }
            protocol.PrintAndLineFeed(output);
        }
    }
}
