#pragma once

#include "util\INIReader.h"
#include "enra\socket.h"
#include "enra\http_request.h"

#include "transport\transport_net.h"
#include "transport\transport_usb.h"

#include "protocols\protocol_pp6900.h"
#include "translator.h"

// System includes
#include <stdio.h>
#if defined(_WIN32) && !defined(_WINRT_DLL)
#include <Windows.h>
#pragma comment(linker, "/SUBSYSTEM:windows /ENTRY:mainCRTStartup")
#endif

#include <string.h>

char* getGUID();

unsigned int G_rest_ID;
std::string G_base_url;
std::string G_order_printer;

#define ACCEPTED_STATUS_VAL 1 // 3 for Micros

int root_main(int argc, char** argv)
{
	G_rest_ID = 0;
	G_base_url = "";
	G_order_printer = "";

	WSADATA WsaDat;
	if (WSAStartup(MAKEWORD(1, 1), &WsaDat) != 0)
	{
		printf("WSA Initialization failed.");
	}
	// Read the INI
	INIReader reader("config.ini");

	if (reader.ParseError() < 0) {
		printf("Can't load 'config.ini'\n");
	}

	if (argc == 3) {
		G_rest_ID = atoi(argv[1]);
		G_order_printer = argv[2];
	}
	else {
		G_rest_ID = reader.GetInteger("base", "rest_id", 158);
		G_order_printer = reader.Get("base", "order_printer", "192.168.192.168:9100");
	}

	G_base_url = reader.Get("base", "base_url", "fmdev.fast-menu.com");

	byte_buffer_t* request_data = byte_buffer_create();
	byte_buffer_t* response_data = byte_buffer_create();
	http_request_t* _request = http_request_create();

	char* g = getGUID();
	// Pair the device
	http_request_init(_request, "config.fast-menu.com", 80);
	http_request_get(_request, (string("/pair/device/") + string(getGUID())).c_str() );
	http_request_response_raw(_request, response_data);

	// Translate print data
	transport* transport_layer = new transport_usb;
	if (!transport_layer->open_device(G_order_printer.c_str()))
	{
		delete transport_layer;
		transport_layer = new transport_net;
		if (!transport_layer->open_device(G_order_printer.c_str()))
		{
			delete transport_layer;
			printf("Unable to connect to any printer! \n");
			Sleep(5000);
			goto finish;
		}
	}

	// Get pending orders
	http_request_init(_request, G_base_url.c_str(), 80);
	http_request_post(_request, (string("/orderService/oService.svc/CheckOrderForPrinting/") + std::to_string(G_rest_ID)).c_str(), request_data, "application/json");
	http_request_response_raw(_request, response_data);
#ifdef LEGACY_WORKFLOW
	// Get print data
	char _order_id[200];
	memset(_order_id, 0, sizeof(_order_id));
	unsigned char lenth_of_order_id = byte_buffer_bytes_left(response_data) > 200 ? 200 : byte_buffer_bytes_left(response_data);
	if (lenth_of_order_id == 0) {
		printf("No pending order.\n");
		Sleep(3000);
		goto finish;
	}
	byte_buffer_read(response_data, _order_id, lenth_of_order_id);

	http_request_init(_request, G_base_url.c_str(), 80);
	http_request_post(_request, (string("/orderService/oService.svc/GetOrderForPrinting/") + string(_order_id)).c_str(), request_data, "application/json");
	byte_buffer_clear(response_data);
	http_request_response_raw(_request, response_data);
#else
	if (!byte_buffer_bytes_left(response_data)) {
		goto finish;
	}
#endif

	byte_buffer_t* print_data = byte_buffer_create();
	printer_protocol* protocol = new protocol_pp6900;
	MarkupTranslator::Translate(protocol, response_data, print_data);
	// Send to printer
	bool order_printed = transport_layer->send_data(print_data);
	byte_buffer_destroy(&print_data);
	delete protocol;
	delete transport_layer;
#ifdef LEGACY_WORKFLOW
	// Change the state of the order
	if (order_printed) {
		// Prepare JSON payload
		rapidjson::StringBuffer s;
		rapidjson::Writer<rapidjson::StringBuffer> writer(s);
		writer.StartObject();
		writer.Key("Id");
		writer.String(_order_id);
		writer.Key("Status");
		writer.Uint(ACCEPTED_STATUS_VAL);
		writer.EndObject();

		byte_buffer_write(request_data, s.GetString(), s.GetSize());
		// Do the request
		http_request_init(_request, G_base_url.c_str(), 80);
		http_request_post(_request, "/orderservice/oService.svc/updateOrder", request_data, "application/json");
		http_request_response_raw(_request, response_data);
		// Note: we ignore the result
	}
#endif
finish:
	http_request_destroy(&_request);
	byte_buffer_destroy(&response_data);
	byte_buffer_destroy(&request_data);

	WSACleanup();

	return 0;
}
char* getGUID() {
	static char S_ENRA_GUID[10];
	FILE* _file = NULL;
	int err = fopen_s(&_file, "enra.guid", "r");
	if (_file == NULL)
		return NULL;
	if (1 != fscanf(_file, "%s", &S_ENRA_GUID))
	{
		int _rnd = rand();
		sprintf(S_ENRA_GUID, "%d", _rnd);
		fprintf(_file, "%d", _rnd);
	}

	fclose(_file);
	return S_ENRA_GUID;
}