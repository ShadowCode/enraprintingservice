#ifndef TRANSPORT_USB_H
#define TRANSPORT_USB_H

#include "../transport.h"
#include <Windows.h>

class transport_usb : public transport
{
	bool bStatus;
	HANDLE	hPrinter;
public:
	bool open_device(const char* id) {
#ifndef _WINRT_DLL
		// Open a handle to the printer. 
		bStatus = OpenPrinter((LPSTR)id, &hPrinter, NULL); 
#endif
		return bStatus;
	}

	void close_device() {
#ifndef _WINRT_DLL
		// Close the printer handle. 
		ClosePrinter(hPrinter);
#endif
	}

	bool send_data(byte_buffer_t* data) {
		if (!bStatus) {
			printf("NO PRINTER\n");
			return false;
		}

		bool result = true;

#ifndef _WINRT_DLL
		// Fill in the structure with info about this "document." 
		DOC_INFO_1 DocInfo;
		DocInfo.pDocName = (LPTSTR)("FastMenu Order"); 
		DocInfo.pOutputFile = NULL;                
		DocInfo.pDatatype = (LPTSTR)("RAW");  
		// Inform the spooler the document is beginning. 
		DWORD dwBytesWritten = 0;
		DWORD dwJob = StartDocPrinter(hPrinter, 1, (LPBYTE)&DocInfo);
		if (dwJob > 0) {
			// Start a page. 
			bStatus = StartPagePrinter(hPrinter);
			if (bStatus) {
				// Send the data to the printer. 
				bStatus = WritePrinter(hPrinter, byte_buffer_contents(data), byte_buffer_size(data), &dwBytesWritten);
				if (dwBytesWritten != byte_buffer_size(data))
					result = false;
				EndPagePrinter(hPrinter);
			}
			// Inform the spooler that the document is ending. 
			EndDocPrinter(hPrinter);
		}
#endif
		return result;
	}
};

#endif // TRANSPORT_USB_H
