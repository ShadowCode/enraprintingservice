#ifndef TRANSPORT_NET_H
#define TRANSPORT_NET_H

#include "../transport.h"
#include "../enra/socket.h"
#include <Windows.h>

class transport_net : public transport
{
	bool bStatus;
	net_socket_t* sock;
public:

	bool open_device(const char* address) {
		// Parse the address
		int ip_port_separator = 0;
		for (uint8 i = 0; i < strlen(address); ++i) {
			if (address[i] == ':') {
				ip_port_separator = i;
				break;
			}
		}
		char ip[255];
		memset(ip, 0, sizeof(ip));
		memcpy(ip, address, ip_port_separator);
		uint16 port = atoi(address + ip_port_separator + 1);
		// Create a socket
		sock = net_socket_create(INVALID_SOCKET);
		// Connect the socket
		bStatus = net_socket_connect(sock, ip, port);
		return bStatus;
	}

	void close_device() {
		net_socket_disconnect(sock);
		// Close the printer handle. 
		net_socket_destroy(&sock);
	}

	bool send_data(byte_buffer_t* data) {
		if (!bStatus) {
			printf("NO PRINTER\n");
			return false;
		}
		return net_socket_write(sock, data);
	}
};

#endif // TRANSPORT_NET_H
