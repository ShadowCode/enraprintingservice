#ifndef TRANSLATOR_H
#define TRANSLATOR_H

#include "enra/buffers.h"
#include "printer_protocol.h"

class MarkupTranslator
{
public:
	static void Translate(printer_protocol* protocol, byte_buffer_t* in, byte_buffer_t* out) {
		protocol->InitPrinter(out);
		protocol->SelectCharecterTable(28, out);
		for (; byte_buffer_bytes_left(in);) {
			uint8 ch = 0;
			if (!byte_buffer_read(in, &ch, 1))
				break;
			if (ch == ';') {
				ch = 0;
				for (; ch != ';' && byte_buffer_read(in, &ch, 1); ) {
					switch (ch)
					{
					case 'b': // Bold
						protocol->SelectPrintMode(32, out);
						break;
					case 'e': // Emphasizes
						protocol->SelectPrintMode(5, out);
						protocol->SetLineSpacing(10, out);
						break;
					case 'd': // Double height
						protocol->SelectPrintMode(16, out);
						break;
					case 'u': // Underline
						protocol->SelectPrintMode(128, out);
						break;
					case 'i': // Invert colors
						protocol->InverseColors(true, out);
						break;
					case '1': // Size 1
						protocol->SelectChSize(80, out);
						break;
					case '2': // Size 2
						protocol->SelectChSize(64, out);
						break;
					case '3': // Size 3
						protocol->SelectChSize(48, out);
						break;
					case '4': // Size 4
						protocol->SelectChSize(32, out);
						break;
					case '5': // Size 5
						protocol->SelectChSize(16, out);
						break;
					case '6': // Size 6
						protocol->SelectChSize(0, out);
						break;
					case 'c': // Center
						//protocol->SelectPrintMode(0, out);
						protocol->AlignContentCenter(out);
						break;
					case 'l': // Left
						//protocol->SelectPrintMode(0, out);
						protocol->AlignContentLeft(out);
						break;
					case 'r': // Right
						//protocol->SelectPrintMode(0, out);
						protocol->AlignContentRight(out);
						break;
					case 'n': // Normal
						protocol->SelectPrintMode(0, out);
						protocol->SelectChSize(0, out);
						protocol->SetLineSpacing(0, out);
						protocol->InverseColors(false, out);
						//protocol->PrintAndReturn(out);
						break;
					case '_': // Bell
						protocol->PulseToCD(200, 50, 1, out);
						protocol->PulseToCD(200, 50, 0, out);
						break;
					case 'z':
						protocol->SelectCharecterTable(28, out);
						break;
					case 'x': // Cut
						protocol->NewLine(out);
						protocol->CutPaper(out);
						break;
					}
				}
			}
			else if (ch == '\n') {
				protocol->NewLine(out);
				protocol->InitPrinter(out);
				protocol->SelectCharecterTable(28, out);
			}
			else {
				if (((int8)ch) < 0) {
					// Transcode
					uint8 _utf_2;
					byte_buffer_read(in, &_utf_2, 1);
					uint8 _transcoded = (uint16)(((ch << 8) + (_utf_2)) - 53392/* + 48*/); // -53392
					if (_transcoded < 164) _transcoded += 192;
					byte_buffer_write(out, &_transcoded, 1);
				}
				else {
					byte_buffer_write(out, &ch, 1);
				}
			}
		}
		protocol->PrintAndLineFeed(out);
	}
};

#endif // TRANSLATOR_H
