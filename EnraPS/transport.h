#ifndef _TRANSPORT_H
#define _TRANSPORT_H

#include "enra/buffers.h"

class transport
{
public:
	virtual bool open_device(const char* id) = 0;
	virtual void close_device() = 0;

	virtual bool send_data(byte_buffer_t* data) = 0;
};

#endif // _TRANSPORT_H
