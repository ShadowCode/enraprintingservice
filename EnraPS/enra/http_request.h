#ifndef HTTP_REQUEST_H
#define HTTP_REQUEST_H

#include "socket.h"
#include "../util/json/document.h"
#include "../util/json/stringbuffer.h"
#include "../util/json/writer.h"

#include <string>
#include <list>
#include <map>

using std::string;
using std::list;
using std::map;

map<string, string> parse_url(const string& url);
map<string, string> parse_post(const string& post);
string url_decode(string& SRC);

// http_request functionality
typedef struct
{
	net_socket_t* sock;
	char base_url[PEER_ADDRESS_MAX_LENGTH + 1];
	uint16 port;
	byte_buffer_t* request;
	byte_buffer_t* response;
} http_request_t;

http_request_t* http_request_create();
void http_request_destroy(http_request_t** self);

#define http_request_init(_http_request, _host, _port) { net_socket_disconnect(_http_request->sock); strncpy(_http_request->base_url, _host, PEER_ADDRESS_MAX_LENGTH); _http_request->port = _port; net_socket_connect(_http_request->sock, _host, _port); }
#define http_request_reset(_http_request) { net_socket_disconnect(_http_request->sock); net_socket_connect(_http_request->sock, _http_request->base_url, _http_request->port); }

bool http_request_get(http_request_t* self, const char* request_url);
bool http_request_post(http_request_t* self, const char* request_url, byte_buffer_t* content, const char* mime_type);
bool http_request_post_multipart(http_request_t* self,
	const char* request_url, byte_buffer_t* content, const char* mime_type,
	FILE* file,
	const char* filename);

bool http_request_response_raw(http_request_t* self, byte_buffer_t* out);
bool http_request_response_file(http_request_t* self, FILE* out);

byte_buffer_t* http_content_url_encoded(byte_buffer_t* out, std::map<std::string, std::string> values);
byte_buffer_t* http_content_json(byte_buffer_t* out, rapidjson::Document& json_doc);
byte_buffer_t* http_content_multipart_encoded(byte_buffer_t* out, std::map<std::string, std::string> values);

#endif // HTTP_REQUEST_H