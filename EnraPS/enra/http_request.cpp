#include <iostream>
#include <fstream>
#include <unordered_map>
#include <ctime>
#include <assert.h>
#include "http_request.h"

#define FILE_UPLOAD_BOUNDRY "----FireStormUploaderFileBoundry"
#define FILE_UPLOAD_MAX_PART_SIZE 2024

map<string, string> parse_post(const string& post)
{
	map<string, string> POST;
	const char separator[] = "&";
	size_t parse_idx;
	size_t pair_sep_idx = 0;
	size_t prev_idx = 0;
	string _post = post + "&";
	while ((parse_idx = _post.find(separator, prev_idx)) != string::npos)
	{
		string line = _post.substr(prev_idx, parse_idx - prev_idx);
		if (!line.length())
		{
			// Headers end
			parse_idx += strlen(separator);
			break;
		}
		pair_sep_idx = line.find('=');
		string parameter = line.substr(0, pair_sep_idx);
		string val = line.substr(pair_sep_idx + 1);
		POST.insert(std::pair<string, string>(parameter, val));
		prev_idx = parse_idx + strlen(separator);
	}
	return POST;
}

map<string, string> parse_url(const string& url)
{
	map<string, string> GET;
	size_t start_of_params = url.find("?");
	if (start_of_params == string::npos)
		start_of_params = 0;
	else
		start_of_params += 1;

	const char separator[] = "&";
	size_t parse_idx;
	size_t pair_sep_idx = 0;
	size_t prev_idx = start_of_params /*+ 1*/;
	string _url = url + "&";
	while ((parse_idx = _url.find(separator, prev_idx)) != string::npos)
	{
		string line = _url.substr(prev_idx, parse_idx - prev_idx);
		if (!line.length())
		{
			// Headers end
			parse_idx += strlen(separator);
			break;
		}
		pair_sep_idx = line.find('=');
		string parameter = line.substr(0, pair_sep_idx);
		string val = line.substr(pair_sep_idx + 1);
		GET.insert(std::pair<string, string>(parameter, val));
		prev_idx = parse_idx + strlen(separator);
	}
	return GET;
}

string url_decode(string& SRC) {
	string ret;
	char ch;
	int i, ii;
	for (i = 0; i<SRC.length(); i++) {
		if (int(SRC[i]) == 37) {
			sscanf(SRC.substr(i + 1, 2).c_str(), "%x", &ii);
			ch = static_cast<char>(ii);
			ret += ch;
			i = i + 2;
		}
		else {
			ret += SRC[i];
		}
	}
	return (ret);
}
/////////////////////////////
// http_request functionality
/////////////////////////////

http_request_t* http_request_create() {
	http_request_t* self = (http_request_t*)malloc(sizeof(http_request_t));
	memset(self, 0, sizeof(http_request_t));

	self->sock = net_socket_create(INVALID_SOCKET);
	self->request = byte_buffer_create(320 + PEER_ADDRESS_MAX_LENGTH);
	self->response = byte_buffer_create(512);

	return self;
}

void http_request_destroy(http_request_t** self) {
	net_socket_destroy(&(*self)->sock);
	byte_buffer_destroy(&(*self)->request);
	byte_buffer_destroy(&(*self)->response);

	free(*self);
	*self = NULL;
}

bool http_request_get(http_request_t* self, const char* request_url)
{
	if (!net_socket_is_connected(self->sock))
		return false;

	byte_buffer_clear(self->request);
	byte_buffer_write_pos(self->request, sprintf((char*)self->request->buffer,
		"GET %s HTTP/1.1\r\n"
		"Host: %s\r\n"
		"Connection: close\r\n\r\n",
		request_url,
		self->base_url));

	return net_socket_write(self->sock, self->request);
}

bool http_request_post(http_request_t* self,
	const char* request_url, byte_buffer_t* content, const char* mime_type)
{
	if (!net_socket_is_connected(self->sock))
		return false;

	byte_buffer_clear(self->request);
	byte_buffer_write_pos(self->request, sprintf((char*)self->request->buffer,
		"POST %s HTTP/1.1\r\n"
		"Host: %s\r\n"
		"Content-Type: %s\r\n"
		"Content-Length: %d\r\n"
		"Connection: close\r\n\r\n",
		request_url,
		self->base_url,
		mime_type,
		byte_buffer_size(content)));
	byte_buffer_write(self->request, content->buffer, byte_buffer_size(content));

	return net_socket_write(self->sock, self->request);
}

bool http_request_post_multipart(http_request_t* self,
	const char* request_url, byte_buffer_t* content, const char* mime_type,
	FILE* file,
	const char* filename)
{
	if (!net_socket_is_connected(self->sock))
		return false;

	// Get file size
	fseek(file, 0L, SEEK_END);
	long file_size = ftell(file);
	fseek(file, 0L, SEEK_SET);

	long total_content_size = file_size; // The raw file size
										 // Prepare parameters

	total_content_size += byte_buffer_size(content);

	// Prepare file header
	byte_buffer_t* file_part_header = byte_buffer_create();
	byte_buffer_write_str(file_part_header, "--");
	byte_buffer_write_str(file_part_header, FILE_UPLOAD_BOUNDRY);
	byte_buffer_write_str(file_part_header, "\r\nContent-Disposition: form-data; name=\"file\"; filename=\"");
	byte_buffer_write_str(file_part_header, filename);
	byte_buffer_write_str(file_part_header, "\"\r\n");
	byte_buffer_write_str(file_part_header, "Content-Type: ");
	byte_buffer_write_str(file_part_header, mime_type);
	byte_buffer_write_str(file_part_header, "\r\n\r\n");

	total_content_size += byte_buffer_size(file_part_header);

	total_content_size += strlen(FILE_UPLOAD_BOUNDRY) + 8; // the size of the footer with the padding

	byte_buffer_t* msg = byte_buffer_create();
	// Prepare headers
	byte_buffer_write_str(msg, "POST ");
	byte_buffer_write_str(msg, request_url);
	byte_buffer_write_str(msg, " HTTP/1.1\r\nHOST: ");
	byte_buffer_write_str(msg, self->base_url);
	byte_buffer_write_str(msg, "\r\nConnection: close");
	byte_buffer_write_str(msg, "\r\nContent-Type: multipart/form-data; boundary=");
	byte_buffer_write_str(msg, FILE_UPLOAD_BOUNDRY);
	byte_buffer_write_str(msg, "\r\nContent-Length: ");
	byte_buffer_write_str(msg, std::to_string(total_content_size).c_str());
	byte_buffer_write_str(msg, "\r\n\r\n");
#ifdef NETWORK_DEBUG_HTTP
	FILE* log = fopen("requet.log", "wb");
	printf("\n%s", byte_buffer_chars(msg));
	fwrite(byte_buffer_chars(msg), 1, byte_buffer_size(msg), log);
#endif
	// Send headers
	if (!net_socket_write(self->sock, msg))
		printf("Unable to send data to server !\n");

	byte_buffer_clear(msg);

	// Send values

#ifdef NETWORK_DEBUG_HTTP
	printf("%s", byte_buffer_chars(params_content));
	fwrite(byte_buffer_chars(params_content), 1, byte_buffer_size(params_content), log);
#endif
	// Send content
	net_socket_write(self->sock, content);

	size_t read_size = 0;
	void* content_part = malloc(FILE_UPLOAD_MAX_PART_SIZE);
	// Send start of content disposition
	net_socket_write(self->sock, file_part_header);

	byte_buffer_destroy(&file_part_header);

	for (; (read_size = fread(content_part, 1, FILE_UPLOAD_MAX_PART_SIZE, file)) != 0; )
	{
		// Prepare content part
		byte_buffer_write(msg, content_part, read_size);
#ifdef NETWORK_DEBUG_HTTP
		printf("%s", byte_buffer_chars(msg));
		fwrite(byte_buffer_chars(msg), 1, byte_buffer_size(msg), log);
#endif
		// Send content
		if (!net_socket_write(self->sock, msg))
			printf("Unable to send data to server !\n");

		byte_buffer_clear(msg);
	}
	free(content_part);

	// Finish the upload
	byte_buffer_write_str(msg, "\r\n--");
	byte_buffer_write_str(msg, FILE_UPLOAD_BOUNDRY);
	byte_buffer_write_str(msg, "--\r\n");
#ifdef NETWORK_DEBUG_HTTP
	printf("%s", byte_buffer_chars(msg));
	fwrite(byte_buffer_chars(msg), 1, byte_buffer_size(msg), log);
	fclose(log);
#endif
	if (!net_socket_write(self->sock, msg))
		printf("Unable to send data to server !\n");

	byte_buffer_destroy(&msg);

	return false;
}

// Get the row response from the connection
// If the content length header is present we read until its fuffiled or connection droped,
// Else we read until the connection is over
bool http_request_response_raw(http_request_t* self, byte_buffer_t* out)
{
	if (!net_socket_is_connected(self->sock))
		return false;

	std::string response;
	byte_buffer_clear(self->response);

	int read = 0;
	int total_read = 0;
	int total_response = INT_MAX;
	int header_length = 0;
	do
	{
		if (net_socket_read(self->sock, self->response))
		{
			// Transfer the read data to the response
			read = byte_buffer_bytes_left(self->response);
			total_read += read;
			response.append((char*)self->response->buffer, read);
			byte_buffer_read_pos(self->response, read);

			// Find HTTP headers length
			if (header_length == 0) {
				header_length = response.find("\x0d\x0a\x0d\x0a") + 4;
			}

			// Find content-length
			int content_length_start = response.find("Content-Length: ");
			if (content_length_start > 0 && total_response == INT_MAX)
			{
				int end = response.find("\r\n", content_length_start);
				std::string _tmp = response.substr(content_length_start + 16, end - (content_length_start + 16));
				total_response = atoi(_tmp.c_str());
			}
		}
		Sleep(1);
	} while (net_socket_is_connected(self->sock) &&
		(total_read - header_length < total_response || total_read == 0));
	response.erase(0, header_length);

	byte_buffer_write(out, response.data(), response.size());
	net_socket_disconnect(self->sock); // Close the connection after the response was read
	if (response.empty())
		return false;
	else
		return true;
}

bool http_request_response_file(http_request_t* self, FILE* out)
{
	if (!net_socket_is_connected(self->sock))
		return false;

	std::string response;
	byte_buffer_clear(self->response);

	int read = 0;
	int total_read = 0;
	int total_response = INT_MAX;
	int header_length = 0;
	do
	{
		if (net_socket_read(self->sock, self->response))
		{
			// Transfer the read data to the response
			read = byte_buffer_bytes_left(self->response);
			total_read += read;
			response.append((char*)self->response->buffer, read);
			//byte_buffer_read_pos(self->response, read);

			// Find content-length
			int content_length_start = response.find("Content-Length: ");
			if (content_length_start > 0 && total_response == INT_MAX)
			{
				int end = response.find("\r\n", content_length_start);
				std::string _tmp = response.substr(content_length_start + 16, end - (content_length_start + 16));
				total_response = atoi(_tmp.c_str());
			}

			// Find HTTP headers length
			if (header_length == 0) {
				header_length = response.find("\x0d\x0a\x0d\x0a") + 4;
				response.erase(0, header_length);
			}

			fwrite(self->response->buffer, 1, byte_buffer_bytes_left(self->response), out);

			response.clear();
			byte_buffer_clear(self->response);
		}
		Sleep(1);
	} while (net_socket_is_connected(self->sock) &&
		(total_read - header_length < total_response || total_read == 0));

	net_socket_disconnect(self->sock); // Close the connection after the response was read
	if (total_response == 0)
		return false;
	else
		return true;
}

byte_buffer_t* http_content_url_encoded(byte_buffer_t* out, std::map<std::string, std::string> values)
{
	for (std::map<std::string, std::string>::iterator it = values.begin(); it != values.end();)
	{
		byte_buffer_write(out, it->first.data(), it->first.size());
		byte_buffer_write(out, "=", 1);
		byte_buffer_write(out, it->second.data(), it->second.size());
		if (++it != values.end()) {
			byte_buffer_write(out, "&", 1);
		}
	}
	return out;
}

byte_buffer_t* http_content_json(byte_buffer_t* out, rapidjson::Document& json_doc)
{
	rapidjson::StringBuffer buffer;
	rapidjson::Writer<rapidjson::StringBuffer> writer(buffer);
	json_doc.Accept(writer);
	byte_buffer_write(out, buffer.GetString(), buffer.GetSize());
	return out;
}

byte_buffer_t* http_content_multipart_encoded(byte_buffer_t* out, std::map<std::string, std::string> values)
{
	for (std::map<std::string, std::string>::iterator it = values.begin(); it != values.end(); ++it)
	{
		// Prepare content part
		byte_buffer_write_str(out, "--");
		byte_buffer_write_str(out, FILE_UPLOAD_BOUNDRY);
		byte_buffer_write_str(out, "\r\nContent-Disposition: form-data; name=\"");
		byte_buffer_write_str(out, it->first.c_str());
		byte_buffer_write_str(out, "\"\r\nContent-Type: text/plane\r\n\r\n");
		byte_buffer_write_str(out, it->second.c_str());
		byte_buffer_write_str(out, "\r\n");
	}
	return out;
}