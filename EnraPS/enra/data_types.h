/*
* ENRA 2.0 2006-2008 all rights reserved.
*	- The External Network Remote Access technology is based
*	  on the Ludmila server logic, and is implemented as a general
*	  networking technology for MMOG's.
*
*	Current file: LoginServer/EnraDataTypes.h
*  Programmer: Veselin Nikolov
*/
#ifndef ENRADATATYPES_H
#define ENRADATATYPES_H

#include <list>
#include <map>
#include <vector>
#include <math.h>
#include <time.h>
#include <string>
#include <assert.h>

#ifdef _WIN32
#define SLEEP(n) Sleep(n)
#else
// posix implementation
#endif

// The basic data types for networking
#ifdef _MSC_VER
typedef signed   __int8     int8;
typedef signed   __int16	int16;
typedef signed   __int32    int32;
typedef signed   __int64    int64;
typedef unsigned __int8		uint8;
typedef unsigned __int16	uint16;
typedef unsigned __int32	uint32;
typedef unsigned __int64	uint64;

#elif defined(_GNUC_)
typedef int8_t				int8;
typedef int16_t				int16;
typedef int32_t				int32;
typedef int64_t				int64;
typedef uint8_t				uint8;
typedef uint16_t			uint16;
typedef uint32_t			uint32;
typedef uint64_t			uint64;

#endif

#define uint16_swap(l)	\
			( ( (l >> 8) &0xFF ) |	\
			  ( (l << 8) & 0xff00 ) )

#define uint32_swap(l)                \
            ( ( ((l) >> 24) & 0x000000FFL ) |       \
              ( ((l) >>  8) & 0x0000FF00L ) |       \
              ( ((l) <<  8) & 0x00FF0000L ) |       \
              ( ((l) << 24) & 0xFF000000L ) )

#define uint64_swap(l)            \
            ( ( ((l) >> 56) & 0x00000000000000FFLL ) |       \
              ( ((l) >> 40) & 0x000000000000FF00LL ) |       \
              ( ((l) >> 24) & 0x0000000000FF0000LL ) |       \
              ( ((l) >>  8) & 0x00000000FF000000LL ) |       \
              ( ((l) <<  8) & 0x000000FF00000000LL ) |       \
              ( ((l) << 24) & 0x0000FF0000000000LL ) |       \
              ( ((l) << 40) & 0x00FF000000000000LL ) |       \
              ( ((l) << 56) & 0xFF00000000000000LL ) )

inline uint32 upper_power_of_two(uint32 v)
{
	v--;
	v |= v >> 1;
	v |= v >> 2;
	v |= v >> 4;
	v |= v >> 8;
	v |= v >> 16;
	v++;
	return v;
}

inline uint64 upper_power_of_two(uint64 v)
{
	v--;
	v |= v >> 1;
	v |= v >> 2;
	v |= v >> 4;
	v |= v >> 8;
	v |= v >> 16;
	v |= v >> 32;
	v++;
	return v;
}

// from code.google.com/p/smhasher/wiki/MurmurHash3
inline uint32 hash(uint32 h)
{
	h ^= h >> 16;
	h *= 0x85ebca6b;
	h ^= h >> 13;
	h *= 0xc2b2ae35;
	h ^= h >> 16;
	return h;
}

// from code.google.com/p/smhasher/wiki/MurmurHash3
inline uint64 hash(uint64 k)
{
	k ^= k >> 33;
	k *= 0xff51afd7ed558ccd;
	k ^= k >> 33;
	k *= 0xc4ceb9fe1a85ec53;
	k ^= k >> 33;
	return k;
}

//from www.cse.yorku.ca/~oz/hash.html
inline unsigned long hash(const char *str)
{
	unsigned long hash = 5381;
	int c;

	for (; c = *str++;)
		hash = ((hash << 5) + hash) + c; /* hash * 33 + c */

	return hash;
}

typedef uint16 socket_guid;
#define SOCKET_GUID_INVALID (~0)
#define FIRE_STORM_MTU 4000

#define PEER_ADDRESS_MAX_LENGTH 260

#endif //ENRADATATYPES_H
