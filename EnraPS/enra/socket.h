#ifndef NET_SOCKET_H
#define NET_SOCKET_H

#pragma comment(lib, "Ws2_32.lib")

#include "../enra/buffers.h"
#include "../enra/thread.h"

#include "Ws2tcpip.h"

/*
	Socket definition
 */

typedef struct net_socket_s net_socket_t;
typedef void (*socket_event_cb_t)(net_socket_t* socket, void* param);

typedef struct {
	socket_event_cb_t callback;
	void* param;
} socket_event_t;

socket_event_t make_socket_event(socket_event_cb_t cb, void* param);

typedef struct net_socket_s
{
	SOCKET sock_fd;
	bool connected;
	bool deleted;
	bool write_buffering;
	sockaddr_in host_address;
	sockaddr_in peer_address;
	byte_buffer_t* write_buffer;
	mutex_t* write_lock;
	// Callback events
	socket_event_t on_connected;
	socket_event_t on_disconnect;
	socket_event_t on_error;
} net_socket_t;


net_socket_t* net_socket_create(int sock_fd);
void net_socket_destroy(net_socket_t** self);

bool net_socket_listen(net_socket_t* self, const char* host, const uint16 port);
bool net_socket_listen(net_socket_t* self, const sockaddr_in& connection_address);
bool net_socket_connect(net_socket_t* self, const char* host, const uint16 port);
bool net_socket_disconnect(net_socket_t* self);

net_socket_t* net_socket_accept(net_socket_t* self);
/** The sockets are non-blocking, if there is data read or writen the return value is true
  */
bool net_socket_read(net_socket_t* self, byte_buffer_t* out);
bool net_socket_write(net_socket_t* self, const byte_buffer_t* data);
bool net_socket_flush(net_socket_t* self);

#define GET_PEER_IP(sock, buf, buflen) (inet_ntop(AF_INET, &sock->peer_address.sin_addr, buf, buflen))
#define GET_PEER_ADDR(sock) (sock->peer_address.sin_addr.s_addr)
#define GET_PEER_PORT(sock) (ntohs(sock->peer_address.sin_port))

#define GET_HOST_IP(sock, buf, buflen) (inet_ntop(AF_INET, &sock->host_address.sin_addr, buf, buflen))
#define GET_HOST_ADDR(sock) (sock->host_address.sin_addr.s_addr)
#define GET_HOST_PORT(sock) (ntohs(sock->host_address.sin_port))

#define net_socket_is_connected(self) (self->connected)

/*
	Broadcast group definition
 */

#define NET_SOCKET_BROADCAST_GROUP_SIZE 10

typedef struct {
	net_socket_t** sockets;
	uint16 group_size;
	uint16 active_clients;
	net_socket_t* _br_sock_end;
} broadcast_group_t;

broadcast_group_t* broadcast_group_create(uint16 size_of_group);
void broadcast_group_destroy(broadcast_group_t** self);

const socket_guid broadcast_group_add(broadcast_group_t* self, net_socket_t* socket);
net_socket_t* broadcast_group_remove(broadcast_group_t* self, net_socket_t* socket);

const socket_guid broadcast_group_duplicate_endpoint(broadcast_group_t* self, net_socket_t* socket);

#define broadcast_group_write(self, data) { for(uint16 _br_i = 0; _br_i < self->group_size; ++_br_i) { if(self->sockets[_br_i] != NULL) net_socket_write(self->sockets[_br_i], data); } }
#define broadcast_group_transfer(from_group, to_group) { for(uint16 _br_i = 0; _br_i < from_group->group_size; ++_br_i) { if(from_group->sockets[_br_i] != NULL) { if(broadcast_group_add(to_group, from_group->sockets[_br_i]) != SOCKET_GUID_INVALID){ from_group->sockets[_br_i] = NULL; from_group->active_clients -= 1;}} } }
#endif // NET_SOCKET_H
