#include "buffers.h"

#define KDefaultSize 0x1000
#define KAllocStepSize 0x200

////////////////////////////////
// Constructors for byte_buffer_t
////////////////////////////////
byte_buffer_t* byte_buffer_create() {
	return byte_buffer_create(KDefaultSize);
}

byte_buffer_t* byte_buffer_create(size_t reserve) {
	byte_buffer_t* self = (byte_buffer_t*)malloc(sizeof(byte_buffer_t));
	memset(self, 0, sizeof(byte_buffer_t));

	self->_owns_buffer = true;
	byte_buffer_reserve(self, reserve);
	return self;
}

byte_buffer_t* byte_buffer_create(byte_buffer_t* copy) {
	byte_buffer_t* self = (byte_buffer_t*)malloc(sizeof(byte_buffer_t));
	memset(self, 0, sizeof(byte_buffer_t));

	self->_owns_buffer = true;
	byte_buffer_write(self, copy->buffer, copy->write_pos);
	return self;
}

byte_buffer_t* byte_buffer_create(const void* data, size_t len) {
	byte_buffer_t* self = (byte_buffer_t*)malloc(sizeof(byte_buffer_t));
	memset(self, 0, sizeof(byte_buffer_t));

	self->_owns_buffer = true;
	byte_buffer_write(self, data, len);
	return self;
}

byte_buffer_t* byte_buffer_create_absorbe(const void* data, size_t len) {
	byte_buffer_t* self = (byte_buffer_t*)malloc(sizeof(byte_buffer_t));
	memset(self, 0, sizeof(byte_buffer_t));

	self->_owns_buffer = false;
	byte_buffer_absorbe(self, data, len);
	return self;
}

void byte_buffer_destroy(byte_buffer_t** self) {
	if ((*self)->_owns_buffer) {
		if ((*self)->buffer != NULL)
			free((*self)->buffer);
	}
	free(*self);
	*self = NULL;
}

////////////////////////////////
// Crunch and reserve for byte_buffer_t
////////////////////////////////
void byte_buffer_reserve(byte_buffer_t* self, size_t reserve) {
	assert(reserve < 10000000);// Note: this is flud protection, if the bufers are not read only writen this will fire
	assert(self->_owns_buffer);

	if (reserve > self->alloc_size) {
		if (self->buffer)
			self->buffer = realloc(self->buffer, reserve);
		else
			self->buffer = malloc(reserve);

		self->alloc_size = reserve;
	}
}

void byte_buffer_reserve(byte_buffer_t* self, size_t reserve, uint8 val) {
	byte_buffer_reserve(self, reserve);
	self->write_pos = reserve;
	memset(self->buffer, val, self->write_pos);
}

size_t byte_buffer_crunch(byte_buffer_t* self) {
	size_t crunch_amount = self->read_pos;
	if (crunch_amount > 0) {
		size_t len = byte_buffer_bytes_left(self);
		memcpy(self->buffer, ((uint8*)self->buffer + self->read_pos), len);
		self->read_pos = 0;
		self->write_pos -= crunch_amount;
	}
	return crunch_amount;
}

void byte_buffer_crunch_reserve(byte_buffer_t* self, size_t needed) {
	if (self->_owns_buffer) {
		size_t size_needed = self->write_pos + needed;
		if (size_needed > self->alloc_size) {
			size_needed -= byte_buffer_crunch(self);
			byte_buffer_reserve(self, size_needed + KAllocStepSize);
		}
	}
}

////////////////////////////////
// Absorbe for byte_buffer_t
////////////////////////////////
void byte_buffer_absorbe(byte_buffer_t* self, byte_buffer_t* src) {
	if (self->_owns_buffer) {
		if (self->buffer)
			free(self->buffer);
	}
	self->buffer = src->buffer;
	self->write_pos = src->write_pos;
	self->read_pos = src->read_pos;
	self->alloc_size = src->alloc_size;
	src->_owns_buffer = false;
	self->_owns_buffer = true;
}

void byte_buffer_absorbe(byte_buffer_t* self, const void* data, size_t len) {
	if (self->_owns_buffer) {
		if (self->buffer)
			free(self->buffer);
	}
	self->buffer = (uint8*)data;
	self->alloc_size = len;
	self->write_pos = len;
	self->read_pos = 0;
	self->_owns_buffer = false;
}

////////////////////////////////
// Copy the byte_buffer_t content
////////////////////////////////
void byte_buffer_copy(byte_buffer_t* const des, const byte_buffer_t* const src) {
	byte_buffer_crunch_reserve(des, src->write_pos);
	byte_buffer_write(des, (uint8*)src->buffer + src->read_pos, byte_buffer_bytes_left(src));
}

////////////////////////////////
// Write and read for byte_buffer_t
////////////////////////////////
void byte_buffer_write(byte_buffer_t* self, const void* data, size_t len) {
	byte_buffer_crunch_reserve(self, len);
	memcpy((uint8*)self->buffer + self->write_pos, data, len);
	self->write_pos += len;
}

bool byte_buffer_read(byte_buffer_t* self, void* data, size_t len) {
	size_t bytes_left = byte_buffer_bytes_left(self);
	if (bytes_left < len) {
		// Note: if we dont have the right size to read just wait, dont partialy read
		/*memcpy( data, (uint8*)self->buffer + self->read_pos, bytes_left );
		self->read_pos += bytes_left;*/
		return false;
	}
	else {
		memcpy(data, (uint8*)self->buffer + self->read_pos, len);
		self->read_pos += len;
		return true;
	}
}