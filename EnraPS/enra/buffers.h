#ifndef BUFFERS_H
#define BUFFERS_H

#include "data_types.h"

typedef struct {
	void* buffer;
	size_t alloc_size;
	size_t write_pos;
	size_t read_pos;
	bool _owns_buffer;
} byte_buffer_t;

byte_buffer_t* byte_buffer_create();
byte_buffer_t* byte_buffer_create(size_t reserve);
byte_buffer_t* byte_buffer_create(byte_buffer_t* copy);
byte_buffer_t* byte_buffer_create(const void* data, size_t len);
byte_buffer_t* byte_buffer_create_absorbe(const void* data, size_t len);

void byte_buffer_destroy(byte_buffer_t** self);

#define byte_buffer_clear(self) {self->write_pos = 0; self->read_pos = 0;}
#define byte_buffer_reset(self) {self->read_pos = 0;}

void byte_buffer_reserve(byte_buffer_t* self, size_t reserve);
void byte_buffer_reserve(byte_buffer_t* self, size_t reserve, uint8 val);
size_t byte_buffer_crunch(byte_buffer_t* self);
void byte_buffer_crunch_reserve(byte_buffer_t* self, size_t needed);

void byte_buffer_absorbe(byte_buffer_t* self, byte_buffer_t* src);
void byte_buffer_absorbe(byte_buffer_t* self, const void* data, size_t len);

void byte_buffer_copy(byte_buffer_t* const des, const byte_buffer_t* const src);

void byte_buffer_write(byte_buffer_t* self, const void* data, size_t len);
bool byte_buffer_read(byte_buffer_t* self, void* data, size_t len);

#define byte_buffer_write_str(self, str) byte_buffer_write(self, str, strlen(str))

#define byte_buffer_size(self) (self->write_pos)
#define byte_buffer_bytes_left(self) (self->write_pos - self->read_pos)

#define byte_buffer_write_pos(self, pos ) { assert( pos <= self->alloc_size ); self->write_pos = pos; }
#define byte_buffer_read_pos(self, pos )  { assert( pos <= self->write_pos  ); self->read_pos = pos;  }
#define byte_buffer_contents(self) ((uint8*)self->buffer)
#define byte_buffer_chars(self) ((char*)self->buffer)

#endif // BUFFERS_H