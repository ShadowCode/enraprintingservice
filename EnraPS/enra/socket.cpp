#include <string>

#ifdef _WIN32
#include <winsock2.h>
#include <Ws2tcpip.h>
#include <Wspiapi.h>
#endif

#include "socket.h"

socket_event_t make_socket_event(socket_event_cb_t cb, void* param) {
	socket_event_t result;
	result.callback = cb;
	result.param = param;
	return result;
}

#define SOCKET_WRITE_BUFFER_BYTES_MAX 6000000

/*
	Socket implementation
 */

net_socket_t* net_socket_create(int sock_fd) {
	net_socket_t* self = (net_socket_t*)malloc(sizeof(net_socket_t));
	memset(self, 0, sizeof(net_socket_t));
	// Set reuse address to true
	if(sock_fd == INVALID_SOCKET) {
		self->sock_fd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	}
	else {
		self->sock_fd = sock_fd;
	}
	u_long arg = 1;
	setsockopt( self->sock_fd, SOL_SOCKET, SO_REUSEADDR, (const char*)&arg, sizeof(u_long) );
	if( self->sock_fd < 0 ) {
		printf("Unable to create socket\n");
	}
	// Set non-blocking mode
	u_long non_block = 1;
	ioctlsocket(self->sock_fd,FIONBIO,&non_block);
	// Create write buffer lock
	self->write_lock = mutex_create();
	// Create write buffer
	self->write_buffer = byte_buffer_create();
	self->write_buffering = true;

	return self;
}

void net_socket_destroy(net_socket_t** self) {
	net_socket_disconnect(*self);
	byte_buffer_destroy(&(*self)->write_buffer);
	mutex_destroy(&(*self)->write_lock);
	free(*self);
	self = NULL;
}

bool net_socket_listen(net_socket_t* self, const char* host, const uint16 port) {
	sockaddr_in sock_address;
	if(!strcmp(host, "0.0.0.0"))
		sock_address.sin_addr.s_addr = htonl(INADDR_ANY);
	else {
		hostent * h = gethostbyname(host);
		if(!h) {
			printf("Could not resolve listen address\n");
			return false;
		}

		memcpy(&sock_address.sin_addr, h->h_addr_list[0], sizeof(in_addr));
	}

	sock_address.sin_family = AF_INET;
	sock_address.sin_port = ntohs(port);

	return net_socket_listen(self, sock_address);
}

bool net_socket_listen(net_socket_t* self, const sockaddr_in& connection_address) {
	if(self->sock_fd < 0) {
		return false;
	}

	if(::bind(self->sock_fd, (const sockaddr*)&connection_address, sizeof(sockaddr_in)) < 0) {
		int err = WSAGetLastError();
		printf("Could not bind\n");
		return false;
	}

	if(listen(self->sock_fd, SOMAXCONN) < 0) {
		printf("Could not listen\n");
		return false;
	}

	self->host_address = connection_address;

	self->connected = true;
	return true;
}

bool net_socket_connect(net_socket_t* self, const char* host, const uint16 port) {
	if(self->connected)
		return false;

	struct addrinfo *result = NULL,
        *ptr = NULL,
        hints;

	ZeroMemory( &hints, sizeof(hints) );
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;

	// Resolve the server address and port
	if( getaddrinfo(host, std::to_string(port).c_str(), &hints, &result) != 0 ) {
		printf("Unable to find host!!!\n");
		return false;
	}

	self->sock_fd = INVALID_SOCKET;
	
	// Attempt to connect to an address until one succeeds
	for(ptr=result; ptr != NULL ;ptr=ptr->ai_next) {

		// Create a SOCKET for connecting to server
		self->sock_fd = socket(ptr->ai_family, ptr->ai_socktype, 
			ptr->ai_protocol);
		if (self->sock_fd == INVALID_SOCKET) {
			printf("socket failed with error: %ld\n", WSAGetLastError());
			return false;
		}

		UINT32 option_value = true;
		setsockopt(self->sock_fd, IPPROTO_TCP, TCP_NODELAY, (char*)&option_value, sizeof(option_value));
		option_value = true;
		setsockopt(self->sock_fd, SOL_SOCKET, SO_REUSEADDR, (char*)&option_value, sizeof(option_value));

		// Connect to server.
		if (::connect( self->sock_fd, ptr->ai_addr, (int)ptr->ai_addrlen) == SOCKET_ERROR) {
			closesocket(self->sock_fd);
			self->sock_fd = INVALID_SOCKET;
			continue;
		}
		break;
	}
	
	freeaddrinfo(result);

	if(self->sock_fd == INVALID_SOCKET)
		return false;

	// Get the peer info 
	int namelen = sizeof(self->peer_address);
	if( getpeername( self->sock_fd, (struct sockaddr*)&self->peer_address, &namelen ) == SOCKET_ERROR )
	{
		printf("getpeername failed with error: %ld\n", WSAGetLastError());
		closesocket(self->sock_fd);
		self->sock_fd = INVALID_SOCKET;
		return false;
	}
	
	// Get host info
	namelen = sizeof(self->host_address);
	if( getsockname( self->sock_fd, (struct sockaddr*)&self->host_address, &namelen ) == SOCKET_ERROR )
	{
		printf("getsockname failed with error: %ld\n", WSAGetLastError());
		closesocket(self->sock_fd);
		self->sock_fd = INVALID_SOCKET;
		return false;
	}

	self->connected = true;
	
	// Set non-blocking mode
	u_long non_block = 1;
	ioctlsocket(self->sock_fd,FIONBIO,&non_block);
	
	if(self->on_connected.callback != NULL) {
		self->on_connected.callback(self, self->on_connected.param);
	}
	return true;
}

bool net_socket_disconnect(net_socket_t* self) {
	if(self->connected) {
		self->connected = false;
		return closesocket(self->sock_fd);
	}
	else {
		return false;
	}
}

net_socket_t* net_socket_accept(net_socket_t* self) {
	if(!(self->connected))
		return NULL;

#ifdef WIN32
	int len = sizeof(sockaddr_in);
#else
	socklen_t len = sizeof(sockaddr_in);
#endif
	net_socket_t* result = NULL;
	// NOTE: Use the mNewPeer for the STUN
	sockaddr_in new_sock_addr;
	SOCKET new_fd = accept(self->sock_fd, (sockaddr*)&new_sock_addr, &len);
	if( new_fd == INVALID_SOCKET ) {
		int nError=WSAGetLastError();
		if(nError != WSAEWOULDBLOCK && nError != 0) {
			if(self->on_error.callback != NULL)
				self->on_error.callback(self, self->on_error.param);
		}
	}
	else {
		// Disable Nagle's algorythm
		// This is bether for streaming as it does not acumulate packets
		// Note: Be cerfull not to flud the client and couse overflow on his side
		UINT32 option_value = true;
		setsockopt(new_fd, IPPROTO_TCP, TCP_NODELAY, (char*) &option_value, sizeof(option_value));
		// Get the socket in abortive shutdown mode
		// Note: we close sockets withowt wayting for FD_CLOSE
		option_value = true; // Don't wait
		setsockopt(new_fd, SOL_SOCKET, SO_DONTLINGER, (char*) &option_value, sizeof(option_value));
		option_value = 0; // No time-out for sending, Hard close
		setsockopt(new_fd, SOL_SOCKET, SO_LINGER, (char*) &option_value, sizeof(option_value));
		//option_value = true; // Enable re-use of port
		//setsockopt(mNewFd, SOL_SOCKET, SO_REUSEADDR, (char*)&option_value, sizeof(option_value));

		result = net_socket_create(new_fd);
		result->peer_address = new_sock_addr;
		result->connected = true;
	}
	return result;
}

bool net_socket_read(net_socket_t* self, byte_buffer_t* out) {
	byte_buffer_crunch_reserve(out, FIRE_STORM_MTU);
	int bytes = recv( self->sock_fd, (char*) out->buffer + out->write_pos, FIRE_STORM_MTU, 0 );
	if( bytes <= 0 ) {// bytes <= 0
		int nError=WSAGetLastError();
		if((nError == 0 && bytes == 0) || (nError != WSAEWOULDBLOCK && nError != 0)) {
			if(self->connected) {
				net_socket_disconnect(self);
				if(self->on_disconnect.callback != NULL) {
					self->on_disconnect.callback(self, self->on_disconnect.param);
				}
			}
			//return true;
		}
		return false;
	}
	else {
		// Note: buffer crunching is going to happen here if max length is reached
		byte_buffer_write_pos(out, out->write_pos + bytes );

		return true;
	}
}

bool net_socket_write(net_socket_t* self, const byte_buffer_t* data) {
#ifdef NETWORK_DEBUG_TRANSPORT
	printf(".[>>]");
#endif
	assert(byte_buffer_bytes_left(data) > 0);
	// Note: buffer crunching is going to prevent buffer overflow
	mutex_acquire_lock(self->write_lock);

	// Direct send if possible
	if(byte_buffer_bytes_left(self->write_buffer) == 0) { 
		int size = byte_buffer_bytes_left(data);
		const char* _data = (const char*)data->buffer + data->read_pos;
		int bytes = send( self->sock_fd, _data, size, 0 );
		//assert(bytes != 0); // local send buffer is filed, remote peer nor accepting data for now

		if( bytes <= 0) {
			int nError=WSAGetLastError();
			if(nError != WSAEWOULDBLOCK && nError != 0) {
				if(self->connected) {
					net_socket_disconnect(self);
					if(self->on_disconnect.callback != NULL) {
						self->on_disconnect.callback(self, self->on_disconnect.param);
					}
				}
				return true;
			}
			if(self->write_buffering) {
				// The client end cant handle the traffic, Append to write buffer
				byte_buffer_write(self->write_buffer, _data, size);
			}
			mutex_release_lock(self->write_lock);
			return false;
		}
		else if(bytes != size) {
			// Write the remainter to the write buffer
			byte_buffer_write(self->write_buffer, _data + bytes, size - bytes);
		}
		mutex_release_lock(self->write_lock);

		return true;
	}
	else { // Buffered send if needed
		// Append to write buffer
		byte_buffer_write(self->write_buffer, (uint8*)data->buffer + data->read_pos, byte_buffer_bytes_left(data));

		int size = byte_buffer_bytes_left(self->write_buffer);
		int bytes = send( self->sock_fd, (const char*)self->write_buffer->buffer + self->write_buffer->read_pos, size, 0 );
		assert(bytes != 0); // local send buffer is filed, remote peer nor accepting data for now

		if( bytes < 0 || byte_buffer_size(self->write_buffer) > SOCKET_WRITE_BUFFER_BYTES_MAX) {
			mutex_release_lock(self->write_lock);
			int nError=WSAGetLastError();
			if(nError != WSAEWOULDBLOCK && nError != 0) {
				if(self->connected) {
					net_socket_disconnect(self);
					if(self->on_disconnect.callback != NULL) {
						self->on_disconnect.callback(self, self->on_disconnect.param);
					}
				}
			}
			return false;
		}
		else {
			// Move the read cursor of the write buffer
			byte_buffer_read_pos(self->write_buffer, self->write_buffer->read_pos + bytes);
			mutex_release_lock(self->write_lock);

			return true;
		}
	}
}

// Tryes to flush the write buffer if everything is flushed it returns true
bool net_socket_flush(net_socket_t* self) {
	int size = byte_buffer_bytes_left(self->write_buffer);
	int bytes = send( self->sock_fd, (const char*)self->write_buffer->buffer + self->write_buffer->read_pos, size, 0 );
	//assert(bytes != 0); // local send buffer is filed, remote peer nor accepting data for now

	if( bytes < 0) {
		mutex_release_lock(self->write_lock);
		int nError=WSAGetLastError();
		if(nError != WSAEWOULDBLOCK && nError != 0) {
			if(self->connected) {
				net_socket_disconnect(self);
				if(self->on_disconnect.callback != NULL) {
					self->on_disconnect.callback(self, self->on_disconnect.param);
				}
			}
		}
		return false;
	}
	else {
		// Move the read cursor of the write buffer
		byte_buffer_read_pos(self->write_buffer, self->write_buffer->read_pos + bytes);
		mutex_release_lock(self->write_lock);

		if(byte_buffer_bytes_left(self->write_buffer)) {
			return false;
		}
		else {
			return true;
		}
	}
}

/*
	Broadcast group implementation
 */

broadcast_group_t* broadcast_group_create(uint16 size_of_group) {
	broadcast_group_t* self = (broadcast_group_t*)malloc(sizeof(broadcast_group_t));
	memset(self, 0, sizeof(broadcast_group_t));
	self->sockets = (net_socket_t**)malloc(sizeof(net_socket_t*) * size_of_group);
	memset(self->sockets, 0, sizeof(net_socket_t*) * size_of_group);
	self->group_size = size_of_group;
	self->_br_sock_end = self->sockets[self->group_size-1];

	return self;
}

void broadcast_group_destroy(broadcast_group_t** self) {
	for(uint16 i = 0; i < (*self)->group_size; ++i) {
		if((*self)->sockets[i] != NULL)
			net_socket_destroy(&(*self)->sockets[i]);
	}
	free((*self)->sockets);
	free((*self));
	*self = NULL;
}

const socket_guid broadcast_group_add(broadcast_group_t* self, net_socket_t* socket) {
	for(uint16 i = 0; i < self->group_size; ++i)
	{ 
		if(self->sockets[i] == NULL)
		{ 
			self->sockets[i] = socket;
			self->active_clients += 1;
			return i;
		}
	}
	return SOCKET_GUID_INVALID;
}

net_socket_t* broadcast_group_remove(broadcast_group_t* self, net_socket_t* socket) {
	for(uint16 i = 0; i < self->group_size; ++i)
	{ 
		if(self->sockets[i] == socket)
		{ 
			self->active_clients -= 1;
			self->sockets[i] = NULL;
			return socket;
		}
	}
	return NULL;
}

const socket_guid broadcast_group_duplicate_endpoint(broadcast_group_t* self, net_socket_t* socket) {

	for (uint16 i = 0; i < self->group_size; ++i)
	{
		if (self->sockets[i] == NULL)
		{
			if (GET_PEER_ADDR(socket) == GET_PEER_ADDR(self->sockets[i])) {
				return i;
			}
		}
	}
	return SOCKET_GUID_INVALID;
}
