#include "thread.h"

thread_t thread_create(thread_entry_point_t entry_point, void* param, bool suspended) {
	thread_t self = CreateThread( NULL, 0, entry_point, param, (suspended) ? CREATE_SUSPENDED : 0, NULL );
	SetThreadPriority(self, THREAD_PRIORITY_TIME_CRITICAL);
	return self;
}

void thread_destroy(thread_t* self) {
	WaitForSingleObject( *self, INFINITE );
	CloseHandle( *self );
	*self = NULL;
}

void thread_start(thread_t self) {
	ResumeThread( self );
	Sleep(1);
}

void thread_stop(thread_t self, bool terminate) {
	if(!terminate)
		WaitForSingleObject( self, INFINITE );
#ifndef _WINRT_DLL
	TerminateThread( self, 0 );
#endif
}

void thread_pause(thread_t self) {
	SuspendThread( self );
}

void thread_resume(thread_t self) {
	ResumeThread( self );
}

mutex_t* mutex_create() {
	mutex_t* self = (mutex_t*)malloc(sizeof(mutex_t));
	InitializeCriticalSection(self);

	return self;
}

void mutex_destroy(mutex_t** self) {
	DeleteCriticalSection(*self);
	free((*self));
	*self = NULL;
}

bool mutex_atempt_acquire(mutex_t* self) {
	return TryEnterCriticalSection( self ) == 0 ? false : true;
}

void mutex_acquire_lock(mutex_t* self) {
	EnterCriticalSection(self);
}

void mutex_release_lock(mutex_t* self) {
	LeaveCriticalSection(self);
}