#ifndef THREAD_H
#define THREAD_H

#include <winsock2.h>

typedef HANDLE thread_t;
typedef LPTHREAD_START_ROUTINE thread_entry_point_t;

thread_t thread_create(thread_entry_point_t entry_point, void* param, bool suspended);
void thread_destroy(thread_t* self);

void thread_start(thread_t self);
void thread_stop(thread_t self, bool terminate);
void thread_pause(thread_t self);
void thread_resume(thread_t self);

typedef CRITICAL_SECTION mutex_t;

mutex_t* mutex_create();
void mutex_destroy(mutex_t** self);

bool mutex_atempt_acquire(mutex_t* self);
void mutex_acquire_lock(mutex_t* self);
void mutex_release_lock(mutex_t* self);


#endif // THREAD_H