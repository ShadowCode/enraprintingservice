﻿#pragma once
#ifndef PROTOCOL_PP6900_H
#define PROTOCOL_PP6900_H

#include "../printer_protocol.h"

class protocol_pp6900 : public printer_protocol
{
public:
	// New line
	byte_buffer_t* NewLine(byte_buffer_t* out) { 
		const uint8 cmd[] = { 0x0A };
		byte_buffer_write(out, cmd, sizeof(cmd));
		return out; 
	}

	// Moves the print position to the next horizontal tab position.
	byte_buffer_t* HorizontalTab(byte_buffer_t* out) { 
		const uint8 cmd[] = { 0x09 };
		byte_buffer_write(out, cmd, sizeof(cmd));
		return out; 
	}

	// Find next black mark
	byte_buffer_t* NextMark(byte_buffer_t* out) { 
		const uint8 cmd[] = { 0x0C };
		byte_buffer_write(out, cmd, sizeof(cmd));
		return out;
	}

	// Calibrate black mark
	byte_buffer_t* CalibrateBlackMark(byte_buffer_t* out) {
		const uint8 cmd[] = { 0x1B, 0x43, 0x41, 0x4C, 0x03 };
		byte_buffer_write(out, cmd, sizeof(cmd));
		return out;
	}

	//Prints the data in the print buffer and feeds one line based on the current line
	//spacing.
	byte_buffer_t* PrintAndLineFeed(byte_buffer_t* out) { 
		const uint8 cmd[] = { 0x0A };
		byte_buffer_write(out, cmd, sizeof(cmd));
		return out;
	}

	// Prints the data in the print buffer collectively and returns to standard mode.
	byte_buffer_t* PrintAndReturn(byte_buffer_t* out) { 
		const uint8 cmd[] = { 0x0C };
		byte_buffer_write(out, cmd, sizeof(cmd));
		return out;
	}

	// When automatic line feed is enabled, this command functions the same as LF;
	byte_buffer_t* PrintAndCarriageReturn(byte_buffer_t* out) { 
		const uint8 cmd[] = { 0x0D };
		byte_buffer_write(out, cmd, sizeof(cmd));
		return out;
	}

	// Transmits the selected printer status specified by n in real-time, according to the
	// following parameters:
	//	n = 1: Transmit printer status
	//	n = 2: Transmit off-line status
	//	n = 3: Transmit error status
	//	n = 4: Transmit paper roll sensor status
	byte_buffer_t* RealTimeStatusTransmission(uint8 n, byte_buffer_t* out) {
		const uint8 cmd[] = { 0x10, 0x04, n };
		byte_buffer_write(out, cmd, sizeof(cmd));
		return out; 
	}

	// In page mode, prints all buffered data in the printing area collectively.
	byte_buffer_t* PrintDataInPageMode(byte_buffer_t* out) { 
		const uint8 cmd[] = { 0x1B, 0x0C };
		byte_buffer_write(out, cmd, sizeof(cmd));
		return out;
	}

	// Sets the character spacing for the right side of the character to [ n ´ horizontal or
	// vertical motion units].
	byte_buffer_t* SetRightSideChSpacing(uint8 n, byte_buffer_t* out) { 
		const uint8 cmd[] = { 0x1B, 0x20, n };
		byte_buffer_write(out, cmd, sizeof(cmd));
		return out;
	}

	//	Selects print mode(s) using n as follows:
	//	Bit Off/On 	Hex Decimal Function
	//	0 	Off 	00 	0 		Character font A (12 ╳ 24).
	//		On 		01 	1 		Character font B (9 ╳ 17).
	//	1 	- 		- 	- 		Undefined.
	//	2 	- 		- 	- 		Undefined.
	//	3 	Off 	00 	0 		Emphasized mode not selected.
	//		On 		08 	8 		Emphasized mode selected.
	//	4 	Off 	00 	0 		Double-height mode not selected.
	//		On 		10 	16 		Double-height mode selected.
	//	5 	Off 	00 	0 		Double-width mode not selected.
	//		On 		20 	32 		Double-width mode selected.
	//	6 	- 		- 	- 		Undefined.
	//	7 	Off 	00 	0 		Underline mode not selected.
	//		On 		80 	128 	Underline mode selected.
	byte_buffer_t* SelectPrintMode(uint8 n, byte_buffer_t* out) {  
		if (n != 0)
			printing_mode |= n;
		else
			printing_mode = n;
		const uint8 cmd[] = { 0x1B, 0x21, printing_mode };
		byte_buffer_write(out, cmd, sizeof(cmd));
		return out;
	}

	// Sets the distance from the beginning of the line to the position at which
	// subsequent characters are to be printed.
	// The distance from the beginning of the line to the print position is
	// [( length + height ╳ 256) ╳(vertical or horizontal motion unit)] inches.
	byte_buffer_t* SetAbsolutePosition(uint8 length, uint8 height, byte_buffer_t* out) { 
		const uint8 cmd[] = { 0x1B, 0x24, length, height };
		byte_buffer_write(out, cmd, sizeof(cmd));
		return out;
	}

	// Selects a bit-image mode using m for the number of dots specified by length and height.
	// Note: To create the data for this command make an extension method handling the dot encoding.
	/*byte_buffer_t* SelectBitImageMode(uint8 m, uint8 length, uint8 height, byte_buffer_t* data, byte_buffer_t* out) {
		byte[] head = (new byte[]{ 0x1B, 0x2A, m, length, height });
		int result_length = head.length + data.length;
		byte[] result = new byte[result_length];
		System.arraycopy(head, 0, result, 0, head.length);
		System.arraycopy(data, 0, result, head.length, data.length);
		return result;
	}*/

	// Selects 1/ 6-inch line (approximately 4.23mm) spacing.
	byte_buffer_t* SetDefaultLineSpacing(byte_buffer_t* out) { 
		const uint8 cmd[] = { 0x1B, 0x32 };
		byte_buffer_write(out, cmd, sizeof(cmd));
		return out;
	}

	// Sets the line spacing to [ n ╳ vertical or horizontal motion unit] inches.
	byte_buffer_t* SetLineSpacing(uint8 n, byte_buffer_t* out) { 
		const uint8 cmd[] = { 0x1B, 0x33, n };
		byte_buffer_write(out, cmd, sizeof(cmd));
		return out;
	}

	// Clears the data in the print buffer and resets the printer mode to the mode that
	// was in effect when the power was turned on.
	byte_buffer_t* InitPrinter(byte_buffer_t* out) { 
		printing_mode = 0;
		const uint8 cmd[] = { 0x1B, 0x40 };
		byte_buffer_write(out, cmd, sizeof(cmd));
		return out;
	}

	// Sets horizontal tab positions.
	// • n specifies the column number for setting a horizontal tab position from the
	// beginning of the line.
	// • k indicates the total number of horizontal tab positions to be set.
	/*byte_buffer_t* SetHorizontalTabPositions(byte_buffer_t* tab_positions, byte_buffer_t* out) {
		byte[] head = (new byte[]{ 0x1B, 0x44 });
		byte[] ending = new byte[]{ 0x00 };
		int result_length = head.length + tab_positions.length + ending.length;
		byte[] result = new byte[result_length];
		System.arraycopy(head, 0, result, 0, head.length);
		System.arraycopy(tab_positions, 0, result, head.length, tab_positions.length);
		System.arraycopy(ending, 0, result, tab_positions.length + head.length, ending.length);
		return result;
	}*/

	// Turns emphasized mode on or off
	byte_buffer_t* EmphasizedMode(bool mode, byte_buffer_t* out)
	{
		uint8 mode_value;
		if (mode)
			mode_value = (uint8)0xFF;
		else
			mode_value = 0x00;

		const uint8 cmd[] = { 0x1B, 0x45, mode_value };
		byte_buffer_write(out, cmd, sizeof(cmd));
		return out;
	}

	// Turns double-strike mode on or off.
	byte_buffer_t* DoubleStrikeMode(bool mode, byte_buffer_t* out)
	{
		uint8 mode_value;
		if (mode)
			mode_value = (uint8)0xFF;
		else
			mode_value = 0x00;

		const uint8 cmd[] = { 0x1B, 0x47, mode_value };
		byte_buffer_write(out, cmd, sizeof(cmd));
		return out;
	}

	// Prints the data in the print buffer and feeds the paper [ n ╳ vertical or horizontal
	// motion unit] inches.
	byte_buffer_t* PrintAndFeedPaperByInches(uint8 inches, byte_buffer_t* out) { 
		const uint8 cmd[] = { 0x1B, 0x4A , inches };
		byte_buffer_write(out, cmd, sizeof(cmd));
		return out;
	}
	// Prints the data in the print buffer and feeds n lines.
	byte_buffer_t* PrintAndFeedPaper(uint8 lines, byte_buffer_t* out) { 
		const uint8 cmd[] = { 0x1B, 0x64 , lines };
		byte_buffer_write(out, cmd, sizeof(cmd));
		return out;
	}

	// Selects character fonts.
	// font = 0, 1, 48, 49
	byte_buffer_t* SelectChFont(uint8 font, byte_buffer_t* out) {
		const uint8 cmd[] = { 0x1B, 0x4D, font };
		byte_buffer_write(out, cmd, sizeof(cmd));
		return out;
	}

	// Selects an international character set n from the following table:
	// n Character
	// 0 U.S.A.
	// 1 France
	// 2 Germany
	// 3 U.K.
	// 4 Denmark
	// 5 Sweden
	// 6 Italy
	// 7 Spain
	// 8 Japan
	// 9 Norway
	// 10 Denmark
	// 11 Spain
	// 12 Latin
	// 13 Korea
	// 14 Slovenia/Croatia
	// 15 Chinese
	// The character sets for Slovenia/Croatia and China are ZJported only in the
	// Simplified Chinese model.
	byte_buffer_t* SelectInternationalLanguage(uint8 n, byte_buffer_t* out) { 
		const uint8 cmd[] = { 0x1B, 0x52, n };
		byte_buffer_write(out, cmd, sizeof(cmd));
		return out;
	}

	// Sets the print starting position based on the current position by using the
	// horizontal or vertical motion unit.
	// • This command sets the distance from the current position to [( nL + nH ╳ 256) ╳
	// horizontal or vertical motion unit]
	byte_buffer_t* SetRelativePosition(uint8 lenght, uint8 height, byte_buffer_t* out) { 
		const uint8 cmd[] = { 0x1B, 0x5C, lenght, height };
		byte_buffer_write(out, cmd, sizeof(cmd));
		return out;
	}

	// Aligns all the data in one line to the specified position
	// Note: this commands are valid only after new line
	byte_buffer_t* AlignContentLeft(byte_buffer_t* out) { 
		const uint8 cmd[] = { 0x1B, 0x61, 0x00 };
		byte_buffer_write(out, cmd, sizeof(cmd));
		return out;
	}
	byte_buffer_t* AlignContentCenter(byte_buffer_t* out) { 
		const uint8 cmd[] = { 0x1B, 0x61, 0x01 };
		byte_buffer_write(out, cmd, sizeof(cmd));
		return out;
	}
	byte_buffer_t* AlignContentRight(byte_buffer_t* out) { 
		const uint8 cmd[] = { 0x1B, 0x61, 0x02 };
		byte_buffer_write(out, cmd, sizeof(cmd));
		return out;
	}

	// Selects a page n from the character code table.
	// n Page
	// 0 PC437 [U.S.A.Standard Europe]
	// 1 Katakana
	// 2 PC850:Multilingual
	// 3 PC860:Portuguese
	// 4 PC863 [Canadian French]
	// 5 PC865:Nodic
	// 6 West Europe
	// 7 Greek
	// 8 Hebrew
	// 9 PC755:East Europe
	// 10 Iran
	// 16 WPC1252
	// 17 PC866:Cyrillic#2
	// 18 PC852:Latin2
	// 19 PC858
	// 20 IranII
	// 21 Latvian
	byte_buffer_t* SelectCharecterTable(uint8 n, byte_buffer_t* out) { 
		const uint8 cmd[] = { 0x1B, 0x74, n };
		byte_buffer_write(out, cmd, sizeof(cmd));
		return out;
	}

	// Prints a NV bit image n using the mode specified by size.
	// 0 Normal
	// 1 Double-width
	// 2 Double-height
	// 3 Quadruple
	byte_buffer_t* PrintNVBitImage(uint8 n, uint8 size, byte_buffer_t* out) { 
		const uint8 cmd[] = { 0x1C, 0x70, n, size };
		byte_buffer_write(out, cmd, sizeof(cmd));
		return out;
	}

	// Select character size
	byte_buffer_t* SelectChSize(uint8 size, byte_buffer_t* out) {
		const uint8 cmd[] = { 0x1D, 0x21, (uint8)(size | size >> 4) };
		byte_buffer_write(out, cmd, sizeof(cmd));
		return out;
	}

	// Turns on or off white/black reverse printing mode.
	byte_buffer_t* InverseColors(bool flag, byte_buffer_t* out)
	{
		uint8 flag_value;
		if (flag)
			flag_value = 0x01;
		else
			flag_value = 0x00;

		const uint8 cmd[] = { 0x1D, 0x42, flag_value };
		byte_buffer_write(out, cmd, sizeof(cmd));
		return out;
	}

	// Sets the left margin using n.
	byte_buffer_t* PrintableAreaLeftMargin(uint8 n, byte_buffer_t* out) { 
		const uint8 cmd[] = { 0x1D, 0x4C, n, n };
		byte_buffer_write(out, cmd, sizeof(cmd));
		return out;
	}

	byte_buffer_t* SetPrinterWidth(uint8 n, byte_buffer_t* out) { 
		const uint8 cmd[] = { 0x1D, 0x4C, n, 0 };
		byte_buffer_write(out, cmd, sizeof(cmd));
		return out;
	}

	// Executes paper cutting.
	byte_buffer_t* CutPaper(byte_buffer_t* out) { 
		const uint8 cmd[] = { 0x1D, 0x56, 0x00 };
		byte_buffer_write(out, cmd, sizeof(cmd));
		return out;
	}

	// Selects the height of the bar code.
	byte_buffer_t* SelectBarCodeHight(uint8 n, byte_buffer_t* out) { 
		const uint8 cmd[] = { 0x1D, 0x68, n };
		byte_buffer_write(out, cmd, sizeof(cmd));
		return out;
	}

	// Selects a bar code system and prints the bar code.
	// 65 UPC-A 11 ≤ n ≤ 12 48 ≤ d ≤ 57
	// 66 UPC-E 11 ≤ n ≤ 12 48 ≤ d ≤ 57
	// 67 JAN13 (EAN13) 12 ≤ n ≤ 13 48 ≤ d ≤ 57
	// 68 JAN8 (EAN8) 7 ≤ n ≤ 8 48 ≤ d ≤ 57
	// 69 CODE39 1 ≤ n ≤ 255
	// 45 ≤ d ≤ 57，
	// 65 ≤ d ≤ 90，
	// d = 32, 36, 37, 43, 45,
	// 46, 47
	// d = 42
	// 70 ITF 1 ≤ n ≤ 255（even number） 48 ≤ d ≤ 57
	// 71 CODABAR 1 ≤ n ≤ 255 48 ≤ d ≤ 57,
	// 65 ≤ d ≤ 68,
	// d = 36, 43, 45, 46,
	// 47, 58
	// 72 CODE93 1 ≤ n ≤ 255 0 ≤ d ≤ 127
	// 73 CODE128 2 ≤ n ≤ 255 0 ≤ d ≤ 127
	/*byte_buffer_t* PrintBarCode(uint8 system, byte_buffer_t* data, byte_buffer_t* out)
	{
		byte[] head = (new byte[]{ 0x1D, 0x6B, system, (byte)data.length });
		int result_length = head.length + data.length;
		byte[] result = new byte[result_length];
		System.arraycopy(head, 0, result, 0, head.length);
		System.arraycopy(data, 0, result, head.length, data.length);
		return result;
	}*/

	// Set the horizontal size of the bar code.
	// Note: 2 <= n <= 6 , Look at the specification
	byte_buffer_t* SetBarCodeWidth(uint8 n, byte_buffer_t* out) { 
		const uint8 cmd[] = { 0x1D, 0x77, n };
		byte_buffer_write(out, cmd, sizeof(cmd));
		return out;
	}

	//Outputs the pulse specified by t1 and t2 to connector pin m to open the chash drawer,
	//	as follows :
	//  m - 0, 48 pin2; 1, 49 pin5
	//  t1 - times x 2ms ON
	//  t2 - times x 2ms OFF
	virtual byte_buffer_t* PulseToCD(uint8 t1, uint8 t2, uint8 m, byte_buffer_t* out) {
		const uint8 cmd[] = { 0x1B, 0x70, m, t1, t2 };
		byte_buffer_write(out, cmd, sizeof(cmd));
		return out;
	}
};

#endif // PROTOCOL_PP6900_H
