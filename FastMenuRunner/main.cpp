
#include <stdio.h>
#include <Windows.h>
#include <time.h>
#include <tchar.h>

#define POS_SERVICE_TIMEOUT_MS 20000

void LogMsg(FILE* log_file, const char* msg)
{
	time_t t = time(0);   // get time now
	struct tm * now = localtime(&t);
	fprintf(log_file, "%d-%d-%d %d:%d:%d | %s\r\n", 
		(now->tm_year + 1900), (now->tm_mon + 1), now->tm_mday, 
		now->tm_hour, now->tm_min, now->tm_sec, 
		msg);
	fflush(log_file);
}

bool ExecuteExternalProcess(TCHAR * path)
{
	STARTUPINFO si;
	PROCESS_INFORMATION pi;

	ZeroMemory(&si, sizeof(si));
	si.cb = sizeof(si);
	ZeroMemory(&pi, sizeof(pi));

	// Start the child process. 
	if (!CreateProcess(NULL,   // No module name (use command line)
		_tcsdup(path),        // Command line
		NULL,           // Process handle not inheritable
		NULL,           // Thread handle not inheritable
		FALSE,          // Set handle inheritance to FALSE
		0,              // No creation flags
		NULL,           // Use parent's environment block
		NULL,           // Use parent's starting directory 
		&si,            // Pointer to STARTUPINFO structure
		&pi)           // Pointer to PROCESS_INFORMATION structure
		)
	{
		printf("CreateProcess failed (%d).\n", GetLastError());
		return false;
	}

	// Wait until child process exits.
	if (WaitForSingleObject(pi.hProcess, POS_SERVICE_TIMEOUT_MS) == WAIT_TIMEOUT)
	{
		TerminateProcess(pi.hProcess, 0);
	}

	// Close process and thread handles. 
	CloseHandle(pi.hProcess);
	CloseHandle(pi.hThread);

	return true;
}

int main(int argc, char** argv)
{
	FILE* log = fopen("runner.log", "ab+");
	printf("FastMenu POS Service is running.\n");
	LogMsg(log, "Runner Started");
	for (;true;) {
		LogMsg(log, "starting fast_menu_pos.exe");
		if (ExecuteExternalProcess(TEXT("fast_menu_pos.exe")))
		{
			LogMsg(log, "OK!");
		}
		else
		{
			LogMsg(log, "Not OK!");
		}
		Sleep(15000);
	}
	LogMsg(log, "Runner Stopped");
	fclose(log);
	return 0;
}
