﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Http;
using Windows.ApplicationModel.Background;
using System.Threading.Tasks;

// The Background Application template is documented at http://go.microsoft.com/fwlink/?LinkID=533884&clcid=0x409

namespace PrintingService
{
    public sealed class StartupTask : IBackgroundTask
    {
        bool serviceRunning = true;
        public async void Run(IBackgroundTaskInstance taskInstance)
        {
            BackgroundTaskDeferral deferral = taskInstance.GetDeferral();
            taskInstance.Canceled += TaskInstance_Canceled;

            var print_engine = new EnraPrint.PrintEngine("http://config.fast-menu.com");
            if (!print_engine.InitializeAsync().GetAwaiter().GetResult())
                goto finish;

            while (serviceRunning)
            {
                await print_engine.UpdateAsync();
                await Task.Delay(15000);
            }

            finish:
            deferral.Complete();
        }

        private void TaskInstance_Canceled(IBackgroundTaskInstance sender, BackgroundTaskCancellationReason reason)
        {
            serviceRunning = false;
        }
    }
}
